from django.contrib import admin
from .models import UnclassifiedItem
from home.admin import ItemAdmin

# Register your models here.
class UnclassifiedItemAdmin(ItemAdmin):
	list_display = ["name", "code", "stock", "location"]


admin.site.register(UnclassifiedItem, UnclassifiedItemAdmin)