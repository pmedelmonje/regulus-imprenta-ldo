from django.db import models
from home.models import *
from utils import utils


class UnclassifiedItem(Item):

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Item sin clasificar"
        verbose_name_plural = "Items sin clasificar"
        db_table = "unclassified_item"