from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from utils import utils
from .models import *
from home.forms import *


class UnclassifiedItemForm(forms.ModelForm):

	class Meta:
		model = UnclassifiedItem
		fields = "__all__"

	location = location_field
	name = custom_name_field
	code = code_field
	stock = stock_field
	description = optional_description_field

	def clean_name(self):
		name = self.cleaned_data.get('name')
		if not self.instance.pk:
			if UnclassifiedItem.objects.filter(name = name).exists():
				raise ValidationError("Ya existe un registro con ese nombre.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(name = name).exists():
					raise ValidationError(f"ya existe un registro en {item_model._meta.verbose_name_plural} con ese nombre.")

		return name				


	def clean_code(self):
		code = self.cleaned_data.get('code')
		if not self.instance.pk:
			if UnclassifiedItem.objects.filter(code = code).exists():
				raise ValidationError("Ya existe un registro con ese código.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(code = code).exists():
					raise ValidationError(f"Ya existe un registro en {item_model._meta.verbose_name_plural} con ese código.")

		return code