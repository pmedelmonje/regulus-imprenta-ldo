from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .models import *
from books.models import *
from machines.models import *
from unclassified_items.models import *
from logbook.models import UnclassifiedItemLog
from .forms import *
from utils.utils import addition_or_substraction
from utils.telegram_bot import send_bot_message


def unclassified_items_home(request):

	unclassified_items = UnclassifiedItem.objects.all()

	context = {
		"title": "Sección de ítems sin clasificar",
		"unclassified_items": unclassified_items
	}

	return render(request, 'unclassified_items/unclassified_items_home.html',
		context)


@login_required
def new_unclassified_item(request):

	if request.method == "POST":
		form = UnclassifiedItemForm(request.POST or None)

		if form.is_valid():
			data = form.cleaned_data

			new_unclassified_item = UnclassifiedItem(
				location = data["location"],
				name = data["name"],
				code = data["code"],
				stock = data["stock"],
				description = data["description"]
			)

			new_unclassified_item.save()

			new_item_log = UnclassifiedItemLog(
				user = request.user,
				unclassified_item = new_unclassified_item,
				log_type = "creacion_item",
				previous_stock = 0,
				updated_stock = new_unclassified_item.stock
			)

			new_item_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha registrado el ítem *{new_unclassified_item.name}*, con un stock inicial de *{new_unclassified_item.stock}*, en la sección de *Ítems sin clasificar*."

			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
				"Ítem registrado correctamente.")

			return HttpResponseRedirect(reverse('unclassified_items_home'))

	else:

		form = UnclassifiedItemForm()

	context = {
		"title": "Registrar nuevo Ítem sin clasificación",
		"form": form
	}

	return render(request, 'unclassified_items/new_unclassified_item.html',
		context)


# Vista para actualizar el stock de un suministro para maquinaria

@login_required
def update_unclassified_item_stock(request, slug):
	unclassified_item = get_object_or_404(UnclassifiedItem, slug = slug)
	previous_stock = unclassified_item.stock

	if request.method == "POST":
		stock_update = int(request.POST.get("stock_update"))

		if stock_update == 0:
			messages.add_message(request, messages.WARNING,
				"Por favor, ingresar un número distinto a cero.")

		elif unclassified_item.stock + stock_update < 0:
			messages.add_message(request, messages.WARNING,
				"El stock debe ser igual o mayor a cero.")

		else:
			unclassified_item.stock += stock_update
			unclassified_item.save()

			new_item_log = UnclassifiedItemLog(
				user = request.user,
				unclassified_item = unclassified_item,
				log_type = addition_or_substraction(previous_stock,
					unclassified_item.stock),
				previous_stock = previous_stock,
				updated_stock = unclassified_item.stock
			)

			new_item_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha actualizado el stock de *{unclassified_item.name}*.\nStock anterior: *{previous_stock}*.\nStock nuevo: *{unclassified_item.stock}*."

			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
				f"Stock de {unclassified_item.name} actualizado correctamente.")

	return HttpResponseRedirect(reverse('unclassified_items_home'))


@login_required
def unclassified_item_detail(request, slug):

	unclassified_item = get_object_or_404(UnclassifiedItem, slug = slug)
	logs = UnclassifiedItemLog.objects.filter(unclassified_item = unclassified_item).all()

	context = {
		"title": "Información de Ítem",
		"unclassified_item": unclassified_item,
		"logs": logs,
	}

	return render(request, 'unclassified_items/unclassified_item_detail.html',
		context)


@login_required
def delete_unclassified_item(request, slug):

	unclassified_item = get_object_or_404(UnclassifiedItem, slug = slug)
	unclassified_item_name = unclassified_item.name

	unclassified_item.delete()

	message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha eliminado el ítem *{unclassified_item.name}*, creado recientemente en la sección de *Items sin clasificar*."
	send_bot_message(message_text)

	messages.add_message(request, messages.INFO,
		f"Se ha eliminado el ítem '{unclassified_item_name}' correctamente.")

	return HttpResponseRedirect(reverse('unclassified_items_home'))