from django.apps import AppConfig


class UnclassifiedItemsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'unclassified_items'
