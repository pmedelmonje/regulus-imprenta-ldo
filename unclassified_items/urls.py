from django.urls import path
from .views import *


urlpatterns = [
	path("", unclassified_items_home, name = "unclassified_items_home"),
	path("new-unclassified-item/", new_unclassified_item,
		name = "new_unclassified_item"),
	path("update-unclassified-item-stock/<str:slug>", 
		update_unclassified_item_stock, 
		name = "update_unclassified_item_stock"),
	path("unclassified-item-detail/<str:slug>", unclassified_item_detail,
		name = "unclassified_item_detail"),
	path("delete-unclassified-item/<str:slug>", delete_unclassified_item,
		name = "delete_unclassified_item")
]