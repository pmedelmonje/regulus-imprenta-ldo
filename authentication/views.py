from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from users.models import User
from .forms import *


def authentication_login(request):
    
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    
    next_url = request.GET.get('next')
    print(next_url)
    
    form = LoginForm(request.POST or None)

    if form.is_valid():
        data = form.cleaned_data

        user = authenticate(
            request,
            username = data['username'],
            password = data['password']
        )
        
        if user is not None:
            login(request, user)

            messages.add_message(request, messages.SUCCESS,
                'Sesión iniciada correctamente')

            if next_url:            
                return redirect(next_url)
            else:
                return redirect('home_index')

        else:
            messages.add_message(request, messages.WARNING,
                'Datos incorrectos. Vuelva a intentar')
            
            return redirect('authentication_login')
        
    else:
        form = LoginForm()

    context = {
        'title': 'Autenticación',
        'form': form,
    }

    return render(request, 'authentication/authentication_login.html', context)


def authentication_logout(request):

    if not request.user.is_authenticated:
        messages.add_message(request, messages.WARNING,
            'No hay una sesión activa.')
        
        return redirect('home_index')
    
    else:
        logout(request)
        messages.add_message(request, messages.INFO,
            'Sesión cerrada exitosamente')
        
        return redirect('home_index')