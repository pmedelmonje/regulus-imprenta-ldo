from django import forms
from users.models import User


class LoginForm(forms.Form):

	username = forms.CharField(
		widget= forms.TextInput(
		attrs= {'class': 'form-control'}, 		
	),
		label = 'Usuario',
	)
	password = forms.CharField(
		widget= forms.PasswordInput(
		attrs= {'class': 'form-control'},
	),
		label = 'Contraseña'
	)

