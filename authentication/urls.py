from django.urls import path
from .views import *


urlpatterns = [
    path('login/', authentication_login, name = 'authentication_login'),
    path('logout/', authentication_logout, name = 'authentication_logout'),
]
