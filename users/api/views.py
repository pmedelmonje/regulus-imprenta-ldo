from rest_framework import status, serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import (AccessToken, RefreshToken,
	BlacklistedToken, OutstandingToken)
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from users.models import User
from .serializers import (CreateUserSerializer, UserSerializer)
from utils import utils
from utils.permissions import (IsActive, IsStaff, IsSupervisor, 
	authorization_header, IsSupervisorOrReadOnly)


class LogoutAPIVIew(APIView):

	permission_classes = [IsAuthenticated]

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		request_body=openapi.Schema(
		        type=openapi.TYPE_OBJECT,
		        properties={
		            'refresh': openapi.Schema(type=openapi.TYPE_STRING),
		        },
		        required=['refresh'],
		    ),
		responses = {200: "Cierre de sesión exitoso"}
	)
	def post(self, request):
		try:
			refresh_token = request.data.get('refresh')
			access_token = request.headers.get('Authorization').split(' ')[1]
			token = RefreshToken(refresh_token)

			token.blacklist()
			
			return Response({"response": "Cierre de sesión exitoso"},
				status = status.HTTP_200_OK)
			
		except Exception as e:
			print(e)
			return Response({"response": "Hubo un error al tratar de cerrar sesión"},
				status = status.HTTP_400_BAD_REQUEST)


class UsersAPIView(APIView):

	permission_classes = [IsSupervisor]

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: UserSerializer}
	)
	def get(self, request):
		users = User.objects.filter(is_active = True).all()
		serializer = UserSerializer(users, many = True)
		return Response(serializer.data,
		status = status.HTTP_200_OK)


class UserAPIView(APIView):

	permission_classes = [IsStaff]

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: UserSerializer}
	)
	def get(self, request, pk, format = None):
		try:
			user = User.objects.get(pk = pk)
		except User.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)

		serializer = UserSerializer(user)
		return Response(serializer.data,
			status = status.HTTP_200_OK)

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: UserSerializer}
	)
	def patch(self, request, pk, format = None):
		try:
			user = User.objects.get(pk = pk)
		except User.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)

		serializer = UserSerializer(user, data = request.data,
			partial = True)
		if serializer.is_valid():
			serializer.save()
			return Response({
				"response": "Información actualizada correctamente"}, 
				status = status.HTTP_200_OK
			)

		return Response(serializer.errors,
			status = status.HTTP_400_BAD_REQUEST)


class CurrentUserAPIView(APIView):

	permission_classes = [IsAuthenticated]

	@swagger_auto_schema(
		manual_parameters = [authorization_header]
	)
	def get(self, request):
		serializer = UserSerializer(request.user)
		return Response(serializer.data)


class UserRegisterAPIView(APIView):

	permission_classes = [IsStaff]

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		request_body = CreateUserSerializer,
		responses = {200: UserSerializer(many=True)}
	)
	def post(self, request):
		serializer = CreateUserSerializer(data = request.data)

		if serializer.is_valid(raise_exception = True):
			user_instance = serializer.save()
			user_serializer = UserSerializer(instance = user_instance)
			return Response(
				data = user_serializer.data,
				status = status.HTTP_200_OK)

		return Response(serializer.errors,
			status = status.HTTP_400_BAD_REQUEST)


#from drf_yasg.utils import swagger_auto_schema
#from rest_framework import serializers, status
