from django.urls import path
from rest_framework_simplejwt.views import (
	TokenObtainPairView, TokenRefreshView, 
)
from .views import (UserRegisterAPIView, UsersAPIView, UserAPIView,
	CurrentUserAPIView, LogoutAPIVIew)

urlpatterns = [
	path('auth/register', UserRegisterAPIView.as_view()),
	path('auth/login', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    path('auth/logout', LogoutAPIVIew.as_view()),
    path('auth/me', CurrentUserAPIView.as_view()),
    path('auth/users', UsersAPIView.as_view()),
    path('auth/users/<int:pk>', UserAPIView.as_view()),
]