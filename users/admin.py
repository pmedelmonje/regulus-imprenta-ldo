from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User


@admin.register(User)
class UserAdminClass(UserAdmin):
    list_display = ["username", "first_name", "last_name",
    "is_active"]
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Información personal', {'fields': 
            ('first_name', 'last_name', 'email')}),
        ('Permisos', {'fields': 
            ('is_active', 'is_staff', 'is_superuser')})
    )