# Generated by Django 4.2.7 on 2024-04-23 00:11

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TelegramSetting',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True, verbose_name='Nombre')),
                ('bot_token', models.CharField(max_length=255, unique=True, verbose_name='Bot Token')),
                ('chat_id', models.CharField(max_length=255, unique=True, verbose_name='Chat ID')),
            ],
            options={
                'verbose_name': 'Ajuste de Telegram',
                'verbose_name_plural': 'Ajustes de Telegram',
                'db_table': 'telegram_setting',
            },
        ),
    ]
