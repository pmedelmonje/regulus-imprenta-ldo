from django.db import models

# Create your models here.

class TelegramSetting(models.Model):

    name = models.CharField(max_length = 255, unique = True,
                            verbose_name = "Nombre")
    bot_token = models.CharField(max_length = 255, unique = True,
                            verbose_name = "Bot Token")
    chat_id = models.CharField(max_length = 255, unique = True,
                            verbose_name = "Chat ID")
    is_active = models.BooleanField(default = True,
                            verbose_name = "Activo")

    
    def __str__(self):
        return self.name

    
    class Meta:
        verbose_name = "Ajuste de Telegram"
        verbose_name_plural = "Ajustes de Telegram"
        db_table = "telegram_setting"