from django.urls import path
from .views import *


urlpatterns = [
	path('', inventory_home, name = "inventory_home"),	
]

locations_urls = [
	path('locations/', inventory_locations, name = "inventory_locations"),
	path('locations/new-location/', inventory_new_location,
		name = "inventory_new_location"),
	path('locations/edit-location/<str:slug>', inventory_edit_location,
		name = "inventory_edit_location"),
	path('locations/delete-location/<str:slug>', inventory_delete_location,
		name = "inventory_delete_location")
]

literature_urls = [
	path('literature/', inventory_literature, name = "inventory_literature"),
	path('literature/book-categories/', inventory_book_categories,
		name = "inventory_book_categories"),
	path('literature/book-categories/new/', inventory_new_book_category,
		name = "inventory_new_book_category"),
	path('literature/book-categories/edit/<str:slug>',
		inventory_edit_book_category,
		name = "inventory_edit_book_category"),
	path('literature/book-categories/delete/<str:slug>', 
		inventory_delete_book_category,
		name = "inventory_delete_book_category"),
	path('literature/new-book/', inventory_new_book,
		name = "inventory_new_book"),
	path('literature/edit-book/<str:slug>', inventory_edit_book,
		name = "inventory_edit_book"),
	path('literature/delete-book/<str:slug>', inventory_delete_book,
		name = "inventory_delete_book"),
	path('literature/new-book-material', inventory_new_book_material,
		name = "inventory_new_book_material"),
	path('literature/edit-book-material/<str:slug>', inventory_edit_book_material,
		name = "inventory_edit_book_material"),
	path('literature/delete-book-material/<str:slug>', inventory_delete_book_material,
		name = "inventory_delete_book_material"),
]

machines_urls = [	
	# vista para listar las marcas
	path('inventory/brands', inventory_brands, name = "inventory_brands"),
	# vista para añadir una marca
	path('inventory/new-brand', inventory_new_brand,
		name = "inventory_new_brand"),
	# vista para editar una marca
	path('inventory/edit-brand/<str:slug>', inventory_edit_brand,
		name = "inventory_edit_brand"),
	# vista para eliminar una marca
	path('inventory/delete-brand/<str:slug>', inventory_delete_brand,
		name = "inventory_delete_brand"),
	# vista para listar las máquinas
	path('inventory/machinery', inventory_machinery,
		name = "inventory_machinery"),
	# vista para añadir una máquina
	path('inventory/new-machine', inventory_new_machine,
		name = "inventory_new_machine"),
	# viata para editar una máquina
	# vista para eliminar una máquina
	# vista para listar los suministros para máquinas
	# vista para registrar un suministro para máquinas
	# vista para editat un suministro para máquinas
	# vista para eliminar un suministro para máquinas
]

unclassified_items_urls = []

urlpatterns = (urlpatterns + locations_urls + literature_urls + 
	machines_urls + unclassified_items_urls)