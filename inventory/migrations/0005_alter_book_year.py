# Generated by Django 4.2.4 on 2023-10-02 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0004_alter_book_year'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='year',
            field=models.PositiveSmallIntegerField(blank=True, default=None, null=True, verbose_name='Año'),
        ),
    ]
