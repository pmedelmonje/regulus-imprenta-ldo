from django.db import models
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from autoslug import AutoSlugField
from users.models import User
from utils import utils

# Create your models here.

name_field = models.CharField(
        max_length =128,
        unique = True,
        validators = [utils.name_validator],
        verbose_name = "Nombre"
        )

autoslug_field = AutoSlugField(populate_from = "name", unique= True,
                        always_update = True)

optional_description_field = models.TextField(null = True, blank = True,
                        verbose_name = "Descripción")

MONTH_CHOICES = (
		('', '---'),
		('enero', 'Enero'), ('febrero','Febrero'),
		('marzo','Marzo'), ('abril','Abril'),
		('mayo','Mayo'), ('junio','Junio'),
		('julio','Julio'), ('agosto','Agosto'),
		('septiembre','Septiembre'), ('octubre','Octubre'),
		('noviembre','Noviembre'), ('diciembre','Diciembre'),
	)


class StockValidationMixin:
    def clean_stock(self):
        if self.stock < 0:
            raise ValidationError("El stock debe ser igual o mayor a cero.")


class Item(models.Model, StockValidationMixin):
    location = models.ForeignKey(Location, on_delete = models.SET_NULL,
        null = True, verbose_name = "Localización")
    code = models.CharField(max_length = 36, unique = True, 
        verbose_name = "Código")
    name = name_field
    slug = autoslug_field
    stock = models.PositiveSmallIntegerField(default = 0, 
                                verbose_name = "Stock")
    description = optional_description_field
    
    class Meta:
        abstract = True
