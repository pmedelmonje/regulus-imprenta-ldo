/*function buttonPressed(id) {
    console.log(id);
    var valorInput = document.getElementById(id).value;
    Swal.fire({
        title: 'Se actualizará el stock. ¿Continuar?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#10ab72',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, actualizar',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.isConfirmed) {
            const form = document.getElementById('updateForm');
            if(form) {
                form.submit();
            }
        }
    });
}
*/

document.addEventListener('DOMContentLoaded', function() {
    const submitButton = document.getElementById('submitButton');
    const updateStockButton = document.getElementById('updateStockButton');
    const updateInfoButton = document.getElementById('updateInfoButton');
    const secondUpdateStockButton = document.getElementById('secondUpdateStockButton');
    const saveForm = document.getElementById('saveForm');
    const logoutButton = document.getElementById('logoutButton');
    const deleteButton = document.getElementById('deleteButton');
    const archiveButton = document.getElementById('archiveButton');
    const unarchiveButton = document.getElementById('unarchiveButton');


    if (submitButton) {
        submitButton.addEventListener('click', function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Se enviarán los datos del formulario. ¿Continuar?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#10ab72',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, guardar',
                cancelButtonText: 'No'
            }).then((result) => {
                if(result.isConfirmed) {
                    saveForm.submit();
                }
            });
        });
    }

    if (updateInfoButton) {
        updateInfoButton.addEventListener('click', function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Actualizar la información del ítem?',
                text: 'Se aconseja guardar solo si se ha modificado la información del ítem.',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#10ab72',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, guardar',
                cancelButtonText: 'No'
            }).then((result) => {
                if(result.isConfirmed) {
                    saveForm.submit();
                }
            });
        });
    }


    if (updateStockButton) {
            updateStockButton.addEventListener('click', function(e) {

                e.preventDefault();

                Swal.fire({
                    title: 'Se actualizará el stock. ¿Continuar?',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#10ab72',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí, actualizar',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        const form = document.getElementById('updateForm');
                        if(form) {
                            form.submit();
                        }
                    }
                });
            });
        }

    if (secondUpdateStockButton) {

            secondUpdateStockButton.addEventListener('click', function(e) {
                e.preventDefault();

                Swal.fire({
                    title: 'Se actualizará el stock. ¿Continuar?',
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#10ab72',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí, actualizar',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        const form = document.getElementById('secondUpdateForm');
                        if(form) {
                            form.submit();
                        }
                    }
                });
            });
        }
    

    if (logoutButton) {
        logoutButton.addEventListener('click', function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Está a punto de cerrar sesión. ¿Continuar?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#10ab72',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Cerrar sesión',
                cancelButtonText: 'Mantener sesión'
            }).then((result) => {
                if(result.isConfirmed) {
                    const logoutUrl = logoutButton.getAttribute('href');
                    window.location.href = logoutUrl;
                }
            });
        });
    }
    

    if (deleteButton) {
        deleteButton.addEventListener('click', function(e) {
            console.log("Botón pulsado");
            e.preventDefault();

            Swal.fire({
                title: '¿Realmente quiere eliminar este registro?',
                text: 'Esta acción no puede deshacerse',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#10ab72',
                confirmButtonText: 'Sí, eliminar',
                cancelButtonText: 'Mantener'
            }).then((result) => {
                if(result.isConfirmed) {
                    const deleteUrl = deleteButton.getAttribute('href');
                    window.location.href = deleteUrl;
                }
            });
        });
    }

    if (archiveButton) {
        archiveButton.addEventListener('click', function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Realmente quiere archivar este registro?',
                text: 'Los registros archivados no estarán visibles posteriormente',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#10ab72',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí',
                cancelButtonText: 'No'
            }).then((result) => {
                if(result.isConfirmed) {
                    const archiveUrl = archiveButton.getAttribute('href');
                    window.location.href = archiveUrl;
                }
            });
        });
    }

    if (unarchiveButton) {
        unarchiveButton.addEventListener('click', function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Desarchivar este registro para que vuelva a ser visible?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#10ab72',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí',
                cancelButtonText: 'No'
            }).then((result) => {
                if(result.isConfirmed) {
                    const unarchiveUrl = unarchiveButton.getAttribute('href');
                    window.location.href = unarchiveUrl;
                }
            });
        });
    }
    
});


$(document).ready( function () {
    $('#booksTable').DataTable({
        responsive: true,
        language: {
            "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
        }
    });
} );


$(document).ready( function () {
    $('#bookMaterialsTable').DataTable({
        responsive: true,
        language: {
            "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
        }
    });
} );


$(document).ready(function () {
    $('.dataTable').each(function (index) {
        $(this).DataTable({
            responsive: true,
            language: {
                "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
            }
        });
    });
});