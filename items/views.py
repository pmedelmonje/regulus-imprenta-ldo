from django.shortcuts import render
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .models import ItemCategory, Item
from .forms import *

# Create your views here.

def object_already_exists(model, pk, name):
    return model.objects.exclude(pk = pk).filter(name = name).exists()


def items_index(request):

    item_categories = ItemCategory.objects.all()
    items = Item.objects.all()

    for category in item_categories:
        category.item_count = Item.objects.filter(item_category = category).count()

    context = {
        'item_categories': item_categories,
        'items': items,
        'title': 'Sección de inventario'
    }

    return render(request, 'items/items_index.html', 
                  context = context)


def items_new_category(request):

    form = CategoryForm(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():

            new_item_category = ItemCategory(
                name = form.cleaned_data['name'],
                description = form.cleaned_data['description']
            )

            new_item_category.save()

            messages.add_message(request, messages.SUCCESS,
                "Categoría creada exitosamente")

            return HttpResponseRedirect('/inventory')

        #else:
        #    form = CategoryForm(request.POST or None)

    context = {
    'form': form,
    'title': 'Creación de categoría',
    }

    return render(request, 'items/items_new_category.html',
                  context)


def items_new_item(request, slug):

    try:
        item_category = ItemCategory.objects.get(slug = slug)
    except ItemCategory.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        form = ItemForm(request.POST or None)

        if form.is_valid():

            data = form.cleaned_data

            new_item = Item(
                item_category = item_category,
                name = data['name'],
                stock = data['stock'],
                year = data['year'],
                month = data['month'],
                description = data['description']
            )

            new_item.save()

            # enviar mensaje por el bot de Telegram

            messages.add_message(request, messages.SUCCESS,
                f'Ítem {new_item.name} creado correctamente.')

            return HttpResponseRedirect(f'/inventory/category-detail/{item_category.slug}')

    else:
        form = ItemForm(initial = {'year': None, 'month': None})

    context = {
        'form': form,
        'title': 'Creación de item',
        'item_category': item_category,
    }

    return render(request, 'items/items_new_item.html', context)


def items_category_detail(request, slug):

    try:
        item_category = ItemCategory.objects.get(slug = slug)
        items = Item.objects.filter(item_category = item_category.pk).all()
    except ItemCategory.DoesNotExist:
        raise Http404

    form = CategoryForm(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            data = form.cleaned_data

            if object_already_exists(ItemCategory, item_category.pk, data['name']):
                messages.add_message(request, messages.ERROR,
                    'Ya existe una categoría con ese nombre')

                return HttpResponseRedirect(reverse('items_category_detail', 
                    args = [item_category.slug]))            

            item_category.name = data['name']
            item_category.description = data['description']

            item_category.save()

            messages.add_message(request, messages.SUCCESS,
                'Información actualizada correctamente.')

            return HttpResponseRedirect(reverse('items_category_detail', 
                args = [item_category.slug]))

    else:
        form = CategoryForm(initial = {'name': None})

    context = {
        'title': f'Categoría "{item_category.name}"',
        'item_category': item_category,
        'items': items,
        'form': form,
    }

    return render(request, 'items/items_category_detail.html',
        context = context)


@login_required
def items_item_detail(request, slug):

    try:
        item = Item.objects.get(slug = slug)

    except Item.DoesNotExist:
        raise Http404

    initial_data = {
        'name': item.name,
        'stock': item.stock,
        'year': item.year,
        'month': item.month,
        'description': item.description
    }

    form = ItemForm(request.POST or None, initial = initial_data)

    if request.method == 'POST':
        pass

    else:
        pass
        #form = ItemForm(initial = {'name': None, 'stock': None})

    context = {
        'title': f'Ítem {item.name}',
        'item': item,
        'form': form,
    }

    return render(request, 'items/items_item_detail.html', context)