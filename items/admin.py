from django.contrib import admin
from .models import Item, ItemCategory
from utils import utils


@admin.register(ItemCategory)
class ItemCategoryAdmin(admin.ModelAdmin):
	list_display = ['name', 'slug', utils.show_description]

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
	list_display = ["name","previous_stock", "stock", utils.show_description]
