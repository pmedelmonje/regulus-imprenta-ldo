from django import forms
from django.core.exceptions import ValidationError
from utils import utils
from .models import ItemCategory, Item


def validate_unique_name(value):
	if Item.objects.filter(name = value).exists():
		raise ValidationError('Ese nombre ya está siendo usado.')


def validate_stock(value):
	if value < 0:
		raise ValidationError('La cantidad no debe ser un número negativo.')


text_field_widget = forms.TextInput(attrs = {'class': 'form-control'})

text_area_widget = forms.Textarea(attrs = 
	{'class': 'form-control',
	'rows': 4})

optional_description_field = forms.CharField(required= False,
		widget= text_area_widget, label= 'Descripción (opcional)')


class CategoryForm(forms.Form):

	name = forms.CharField(max_length = 50, 
		required = True,
		validators = [utils.name_validator, validate_unique_name], 
		widget = text_field_widget,
		label= 'Nombre')
	description = optional_description_field


class ItemForm(forms.Form):

	name = forms.CharField(required= True, 
		validators = [utils.name_validator, validate_unique_name], 
		widget = text_field_widget,	
		label = 'Nombre')	
	stock = forms.IntegerField(required = True,
		validators = [validate_stock],
		widget = forms.NumberInput(attrs= {'class': 'form-control'}),
		label = 'Cantidad')
	year = forms.CharField(max_length = 4, required= False,
		widget = text_field_widget, label = 'Año')
	month = forms.ChoiceField(required= False, choices = Item.MONTH_CHOICES,
		widget= forms.Select(attrs= {'class': 'form-control'}), 
		label = 'Mes')
	description = optional_description_field


	# def clean_name(self):
	# 	name = self.cleaned_data.get('name')
	# 	# Crear una función que se pueda reutilizar en otros campos
	# 	if Item.objects.filter(name = name).exists():
	# 		raise forms.ValidationError('Ese nombre ya está siendo utilizado')

	# 	return name
	
	def clean(self):
		cleaned_data = super().clean()
		year = cleaned_data.get('year')	
		month = cleaned_data.get('month')

		if month and not year:
			self.add_error('month', 
				'Se requiere ingresar un año para poder ingresar un mes')