from rest_framework import serializers
from items.models import Item
from categories.api.serializers import CategorySerializer
from utils import utils


class CreateItemSerializer(serializers.ModelSerializer):
	name = serializers.CharField(validators = [utils.name_validator])
	class Meta:
		model = Item
		fields = ['category', 'name', 'stock', 'description', 
		'year', 'month']

	def validate_stock(self, value):
		if value < 0:
			raise serializers.ValidationError(
				"El stock debe ser igual o mayor a cero.")
		return value
	
	def validate_year(self, value):
		str_years = [str(year) for year in range(1900, 2031)]

		if value and value not in str_years:
			raise serializers.ValidationError(
				"Por favor, ingresar un año entre 1930 y 2030.")
		return value
		
	def validate_month(self, value):
		str_months = [str(month) for month in range(1,13)]				
		
		if value and value not in str_months:
			raise serializers.ValidationError(
				"Mes no válido. Se debe ingresar el número del mes (del 1 al 12).")
	
		year = self.initial_data.get('year')	
		if value and not year:
			raise serializers.ValidationError(
				"Se requiere ingresar un año para ingresar un mes.")
		return value


class ItemSerializer(serializers.ModelSerializer):
	category = CategorySerializer()	
	class Meta:
		model = Item
		fields = ['name', 'slug', 'previous_stock', 'stock', 'description',
		'category', 'year', 'month']


class SearchItemSerializer(serializers.Serializer):
	category = serializers.IntegerField(required = False)
	year = serializers.CharField(max_length = 4, required = False)
	month = serializers.CharField(max_length = 2, required = False)