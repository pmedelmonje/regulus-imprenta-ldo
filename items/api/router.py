from django.urls import path
from .views import (ItemsAPIView, ItemAPIView, ItemsPerCategoryAPIView,
	ItemsSearchAPIView)

urlpatterns = [
	path('items/', ItemsAPIView.as_view()),
    path('items/search/', ItemsSearchAPIView.as_view()),
	path('items/<str:slug>/', ItemAPIView.as_view()),
	path('items/category/<str:slug>/', ItemsPerCategoryAPIView.as_view()),
]