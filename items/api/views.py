from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.exceptions import ValidationError
from drf_yasg.utils import swagger_auto_schema
from .serializers import (ItemSerializer, CreateItemSerializer,
	SearchItemSerializer)
from items.models import Item
from categories.models import Category
#from records.models import RecordType
from utils import telegram_bot
from utils.permissions import (authorization_header, IsSupervisor,
	IsSupervisorOrReadOnly)
from utils.pagination import CustomPagination


class ItemAPIView(APIView):

	permission_classes = [IsAuthenticated]

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: ItemSerializer()}
	)
	def get(self, request, slug, format = None):
		try:
			item = Item.objects.get(slug = slug)
		except Item.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)
		serializer = ItemSerializer(item)
		return Response(serializer.data)

	
	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		request_body = CreateItemSerializer(),
		responses = {200: "Información actualizada correctamente"}
	)
	def patch(self, request, slug, format = None):
		try:
			item = Item.objects.get(slug = slug)
		except Item.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)

		serializer = ItemSerializer(item, data = request.data, 
			partial = True)
		if serializer.is_valid():
			serializer.save()
			return Response(
				{"response": "Información actualizada correctamente"},
				status = status.HTTP_200_OK
			)

		return Response(serializer.errors,
			status = status.HTTP_400_BAD_REQUEST)


class ItemsAPIView(APIView):

	permission_classes = [IsAuthenticated]
	pagination_class = CustomPagination
	

	@swagger_auto_schema(
		manual_parameters = [authorization_header],		
		responses = {200: ItemSerializer(many = True)}
	)
	def get(self, request, format = None):
		items = Item.objects.all()
		paginated_items = self.pagination_class().paginate_queryset(items, request)
		serializer = ItemSerializer(paginated_items, many = True)
		return Response(serializer.data, 
			status = status.HTTP_200_OK)


	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		request_body = CreateItemSerializer(),
		responses = {201: ItemSerializer()}
	)
	def post(self, request, format = None):
		serializer = CreateItemSerializer(data = request.data)
		try:
			serializer.is_valid(raise_exception = True)
			item_name = serializer.validated_data["name"]
			item_stock = serializer.validated_data["stock"]
			item_category = serializer.validated_data["category"]
			item_instance = serializer.save()
			message = f"Se ha registrado el Item *{item_name}*, con un stock inicial de *{item_stock}*, en la categoría *{item_category}*."
			telegram_bot.send_bot_message(message)
			item_serializer = ItemSerializer(instance = item_instance)
			return Response(data = item_serializer.data,
		   status = status.HTTP_201_CREATED)
		
		except ValidationError as e:
			return Response(e.detail,
		   status = status.HTTP_400_BAD_REQUEST)
		# if serializer.is_valid():
		# 	if serializer.validated_data["stock"] < 0:
		# 		return Response({'stock': 'El stock inicial debe ser mayor a cero.'},
		# 			status = status.HTTP_400_BAD_REQUEST)
		# 	else:

		# 		item_name = serializer.validated_data["name"]
		# 		item_stock = serializer.validated_data["stock"]
		# 		item_category = serializer.validated_data["category"]
		# 		item_instance = serializer.save()

		# 		message = f"Se ha registrado el Item *{item_name}*, con un stock inicial de *{item_stock}*, en la categoría *{item_category}*."

		# 		telegram_bot.send_bot_message(message)

		# 		item_serializer = ItemSerializer(instance = item_instance)
		# 		return Response(
		# 			data = item_serializer.data,
		# 			status = status.HTTP_201_CREATED)
		# return Response(serializer.errors,
		# 	status = status.HTTP_400_BAD_REQUEST)
	

class ItemsPerCategoryAPIView(APIView):

	permission_classes = [IsAuthenticated]
	pagination_class = CustomPagination
	
	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: ItemSerializer()}
	)
	def get(self, request, slug, format = None):
		try:
			category = Category.objects.get(slug = slug)
			items = Item.objects.filter(category = category).all()
			serializer = ItemSerializer(items, many = True)
			return Response(serializer.data)
		except Category.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)


class ItemsSearchAPIView(APIView):

	permission_classes = [IsAuthenticated]
	pagination_class = CustomPagination

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		request_body = SearchItemSerializer(),
		responses = {200: ItemSerializer(many = True)}
	)
	def post(self, request):
		serializer = SearchItemSerializer(data = request.data,
			partial = True)
		if serializer.is_valid(raise_exception = True):
			category = serializer.validated_data.get('category')
			year = serializer.validated_data.get('year')
			month = serializer.validated_data.get('month')

			items = Item.objects.all()

			if category is not None:
				items = items.filter(category = category)

			if year is not None:
				items = items.filter(year = year)

			if month is not None:
				items = items.filter(month = month)

			if not items.exists():
				return Response({"response": "No hay resultados"},
					status = status.HTTP_200_OK)

			serialized_items = ItemSerializer(items,
				many = True)

			return Response(serialized_items.data, 
				status = status.HTTP_200_OK)

		return Response(serializer.errors,
			status = status.HTTP_400_BAD_REQUEST)