from django.db import models
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from autoslug import AutoSlugField
from users.models import User
from utils import utils


class Category(models.Model):
	name = models.CharField(max_length = 30, unique = True,
		validators = [utils.name_validator], verbose_name = "Nombre")
	slug = AutoSlugField(populate_from = 'name',
		always_update = True, verbose_name = "Slug")
	description = models.TextField(blank = True,
		default = "", verbose_name = "Descripción")
	

	def __str__(self):
		return self.name
	
	class Meta:
		verbose_name = "Categoría"
		verbose_name_plural = "Categorías"
		db_table = "category"
		ordering = ("name", )


class SubCategory(models.Model):
	category = models.ForeignKey(Category, on_delete = models.PROTECT,
			null = True, verbose_name = "Categoría")
	name = models.CharField(max_length = 30, unique = True,
		validators = [utils.name_validator], verbose_name = "Nombre")
	slug = AutoSlugField(populate_from = 'name',
		always_update = True, verbose_name = "Slug")
	description = models.TextField(blank = True,
		default = "", verbose_name = "Descripción")

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = "Subcategoría"
		verbose_name_plural = "Subcategorías"
		db_table = "subcategory"
		ordering = ("name", )


class Item(models.Model):

	MONTH_CHOICES = (
		('', '---'),
		('enero', 'Enero'), ('febrero','Febrero'),
		('marzo','Marzo'), ('abril','Abril'),
		('mayo','Mayo'), ('junio','Junio'),
		('julio','Julio'), ('agosto','Agosto'),
		('septiembre','Septiembre'), ('octubre','Octubre'),
		('noviembre','Noviembre'), ('diciembre','Diciembre'),
	)

	sub_category = models.ForeignKey(SubCategory, on_delete = models.PROTECT,
		null = True, verbose_name = 'Subcategoría')
	name = models.CharField(
		max_length = 255, 
		unique = True,
		verbose_name= 'Nombre'
		)
	slug = AutoSlugField(populate_from = 'name',
		always_update = True, unique = True)
	previous_stock = models.IntegerField(default = 0, 
									  verbose_name= 'Stock previo')
	stock = models.IntegerField(default = 0, 
							 verbose_name= 'Stock')
	year = models.CharField(max_length = 4 ,blank = True, null = True,
						 verbose_name= 'Año')
	month = models.CharField(max_length = 10, blank = True, null = True, 
						  choices= MONTH_CHOICES, default = None, 
						  verbose_name= 'Mes')
	description = models.TextField(blank = True, verbose_name= 'Descripción')

	def __str__(self):
		return self.name
	

	### ¿Dejo las validaciones en el modelo o las hago en las vistas?

	### Validaciones en el modelo
	def clean_stock(self):
		if self.stock < 0:
			raise ValidationError("El stock debe ser igual o mayor a cero.")
		
	def clean_year(self):
		str_years = [str(year) for year in range(1900, 2031)]

		if self.year and self.year not in str_years:
			raise ValidationError(
				"Por favor, ingresar un año entre 1930 y 2030.")
	
	def clean_month(self):
		
		if self.month is not None and self.year is None:
			raise ValidationError(
				"Se requiere ingresar un año para ingresar un mes.")


	def clean(self):
		super().clean()
		self.clean_stock()
		self.clean_year()
		self.clean_month()

	### /Validaciones en el modelo

	class Meta:
		verbose_name = "Item"
		verbose_name_plural = "Items"
		db_table = "item"
		ordering = ("name", )


### Esto sí queda en el modelo
@receiver(pre_save, sender = Item)
def update_previous_stock(sender, instance, **kwargs):
	if instance.pk:
		try:
			item = Item.objects.get(pk = instance.pk)
			
			if item.stock != instance.stock:
				instance.previous_stock = item.stock
		except Item.DoesNotExist:
			pass