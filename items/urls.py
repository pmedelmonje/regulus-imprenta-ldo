from django.urls import path
from .views import *

urlpatterns = [
    path('', items_index, name = 'items_index'),
    path('new-item/<str:slug>', items_new_item,
        name = 'items_new_item'),
    path('item-detail/<str:slug>', items_item_detail,
        name = 'items_item_detail'),
]
