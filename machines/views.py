from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .models import *
from books.models import *
from machines.models import *
from logbook.models import MachineLog, MachinesSupplyLog
from unclassified_items.models import *
from .forms import *
from utils.utils import addition_or_substraction
from utils.telegram_bot import send_bot_message

### vista para listar las marcas

@login_required
def machines_brands(request):
	brands = Brand.objects.all()

	for brand in brands:
		brand.item_count = 0
		brand.item_count += Machine.objects.filter(brand = brand.pk).count()
		brand.item_count += MachinesSupply.objects.filter(brand = brand.pk).count()

	context = {
		"title": "Sección de marcas",
		"brands": brands
	}

	return render(request, 'machines/machines_brands.html',
		context)


### vista para registrar una marca

@login_required
def machines_new_brand(request):

	if request.method == "POST":

		form = BrandForm(request.POST or None)

		if form.is_valid():
			data = form.cleaned_data

			new_brand = Brand(name = data["name"])
			new_brand.save()

			messages.add_message(request, messages.SUCCESS,
				"Marca registrada correctamente.")

			return HttpResponseRedirect(reverse('machines_brands'))

	else:
		form = BrandForm()

	context = {
		"title": "Registrar marca",
		"form": form
	}

	return render(request, 'machines/machines_new_brand.html',
		context)


### vista para editar una marca

@login_required
def machines_edit_brand(request, slug):

	brand = get_object_or_404(Brand, slug = slug)
	item_count = 0
	item_count += Machine.objects.filter(brand = brand.pk).count()
	item_count += MachinesSupply.objects.filter(brand = brand.pk).count()

	initial = {"name": brand.name}

	if request.method == "POST":
		form = BrandForm(request.POST or None,
			initial = initial, instance = brand)

		if form.is_valid():
			data = form.cleaned_data

			brand.name = data["name"]

			brand.save()

			messages.add_message(request, messages.SUCCESS,
				"Información actualizada correctamente")

			return HttpResponseRedirect(reverse('machines_brands'))

	else:
		form = BrandForm(instance = brand)

	context = {
		"title": "Editar Marca",
		"brand": brand,
		"item_count": item_count,
		"form": form
	}

	return render(request, 'machines/machines_edit_brand.html',
		context)


### vista para eliminar una marca

@login_required
def machines_delete_brand(request, slug):

	brand = get_object_or_404(Brand, slug = slug)
	brand_name = brand.name

	brand.delete()

	messages.add_message(request, messages.INFO,
		f"Marca {brand_name} eliminada correctamente.")

	return HttpResponseRedirect(reverse('machines_brands'))


### vista para listar las máquinas

def machines_home(request):

	""" Vista para listar las máquinas """

	machines = Machine.objects.all()
	machines_supplies = MachinesSupply.objects.all()

	context = {
		"title": "Sección Maquinaria",
		"machines": machines,
		"machines_supplies": machines_supplies,
	}

	return render(request, 'machines/machines_home.html',
		context)


# vista para registrar una máquina

@login_required
def machines_new_machine(request):

	if request.method == "POST":

		form = MachineForm(request.POST or None)

		if form.is_valid():
			data = form.cleaned_data

			new_machine = Machine(
				location = data["location"],
				name = data["name"],
				code = data["code"],
				stock = data["stock"],
				brand = data["brand"],
				model = data["model"],
				description = data["description"]
			)

			new_machine.save()

			new_machine_log = MachineLog(
				user = request.user,
				machine = new_machine,
				log_type = "creacion_item",
				previous_stock = 0,
				updated_stock = new_machine.stock
			)

			new_machine_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha registrado el ítem *{new_machine.name}* en la sección de *Maquinaria*, con un stock inicial de *{new_machine.stock}*"

			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
				f"Máquina {new_machine.name} registrada correctamente.")

			return HttpResponseRedirect(reverse('machines_home'))

	else:
		form = MachineForm()

	context = {
		"title": "Registrar nueva Máquina",
		"form": form
	}

	return render(request, 'machines/machines_new_machine.html',
		context)


# Vista para actualizar el stock de una máquina

@login_required
def update_machine_stock(request, slug):
	machine = get_object_or_404(Machine, slug = slug)
	previous_stock = machine.stock

	if request.method == "POST":
		stock_update = int(request.POST.get("stock_update"))

		if stock_update == 0:
			messages.add_message(request, messages.WARNING,
				"Por favor, ingresar un número distinto a cero.")

		elif machine.stock + stock_update < 0:
			messages.add_message(request, messages.WARNING,
				"El stock debe ser igual o mayor a cero.")

		else:
			machine.stock += stock_update
			machine.save()

			new_machine_log = MachineLog(
				user = request.user,
				machine = machine,
				log_type = addition_or_substraction(previous_stock, 
					machine.stock),
				previous_stock = previous_stock,
				updated_stock = machine.stock
			)

			new_machine_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha actualizado el stock de *{machine.name}*.\nStock previo: *{previous_stock}*.\nNuevo stock: *{machine.stock}*."
			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
				f"Stock de {machine.name} actualizado correctamente.")

	return HttpResponseRedirect(reverse('machines_home'))

# vista para editar una máquina

@login_required
def machines_machine_detail(request, slug):

	machine = get_object_or_404(Machine, slug = slug)
	logs = MachineLog.objects.filter(machine = machine).all()

	context = {
		"title": "Información de Máquina",
		"machine": machine,
		"logs": logs,
	}

	return render(request, 'machines/machines_machine_detail.html',
		context)

# vista para eliminar una máquina

@login_required
def machines_delete_machine(request, slug):

	machine = get_object_or_404(Machine, slug = slug)
	machine_name = machine.name

	machine.delete()

	messages.add_message(request, messages.INFO,
		f"Máquina {machine_name} eliminada correctamente.")

	return HttpResponseRedirect(reverse('machines_home'))

# vista para registrar un suministro para máquinas

@login_required
def machines_new_machines_supply(request):

	if request.method == "POST":
		form = MachinesSupplyForm(request.POST or None)

		if form.is_valid():
			data = form.cleaned_data

			new_machines_supply = MachinesSupply(
				location = data["location"],
				brand = data["brand"],
				machine = data["machine"],
				name = data["name"],
				code = data["code"],
				stock = data["stock"],
				description = data["description"]
			)

			new_machines_supply.save()

			new_machines_supply_log = MachinesSupplyLog(
				user = request.user,
				machines_supply = new_machines_supply,
				log_type = "creacion_item",
				previous_stock = 0,
				updated_stock = new_machines_supply.stock
			)

			new_machines_supply_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha registrado el ítem *{new_machines_supply.name}* en la sección de *Maquinaria*, con un stock inicial de *{new_machines_supply.stock}*"

			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
				f"Suministro '{new_machines_supply.name}' registrado correctamente")

			return HttpResponseRedirect(reverse('machines_home'))

	else:
		form = MachinesSupplyForm()

	context = {
		"title": "Registrar nuevo Suministro",
		"form": form
	}

	return render(request, 'machines/machines_new_machines_supply.html',
		context)


# Vista para actualizar el stock de un suministro para maquinaria

@login_required
def update_machines_supply_stock(request, slug):
	machines_supply = get_object_or_404(MachinesSupply, slug = slug)
	previous_stock = machines_supply.stock

	if request.method == "POST":
		stock_update = int(request.POST.get("stock_update"))

		if stock_update == 0:
			messages.add_message(request, messages.WARNING,
				"Por favor, ingresar un número distinto a cero.")

		elif machines_supply.stock + stock_update < 0:
			messages.add_message(request, messages.WARNING,
				"El stock debe ser igual o mayor a cero.")

		else:
			machines_supply.stock += stock_update
			machines_supply.save()

			new_machines_supply_log = MachinesSupplyLog(
				user = request.user,
				machines_supply = machines_supply,
				log_type = addition_or_substraction(previous_stock,
					machines_supply.stock),
				previous_stock = previous_stock,
				updated_stock = machines_supply.stock
			)

			new_machines_supply_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha actualizado el stock de *{machines_supply.name}*.\nStock previo: *{previous_stock}*.\nNuevo stock: *{machines_supply.stock}*."
			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
				f"Stock de {machines_supply.name} actualizado correctamente.")

	return HttpResponseRedirect(reverse('machines_home'))

# vista para editar un suministro para máquinas

@login_required
def machines_machines_supply_detail(request, slug):

	machines_supply = get_object_or_404(MachinesSupply, slug = slug)
	logs = MachinesSupplyLog.objects.filter(machines_supply = machines_supply).all()

	context = {
		"title": "Información de Suministro para Maquinaria",
		"machines_supply": machines_supply,
		"logs": logs,
	}

	return render(request, 'machines/machines_machines_supply_detail.html',
		context)

# vista para eliminar un suministro para máquinas

@login_required
def machines_delete_machines_supply(request, slug):
	machines_supply = get_object_or_404(MachinesSupply, slug = slug)
	machines_supply_name = machines_supply.name

	machines_supply.delete()

	message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha eliminado el registro de *{machines_supply.name}*, creado recientemente."
	send_bot_message(message_text)

	messages.add_message(request, messages.INFO,
		f"Suministro '{machines_supply_name}' eliminado correctamente.")

	return HttpResponseRedirect(reverse('machines_home'))