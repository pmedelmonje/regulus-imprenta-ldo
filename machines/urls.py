from django.urls import path
from .views import *

urlpatterns = [
	path('', machines_home, name = "machines_home"),
	path("new-machine/", machines_new_machine,
		name = "machines_new_machine"),
	path("machine-detail/<str:slug>", machines_machine_detail,
		name = "machines_machine_detail"),
	path("update-machine-stock/<str:slug>", update_machine_stock,
		name = "update_machine_stock"),
	path("delete-machine/<str:slug>", machines_delete_machine,
		name = "machines_delete_machine"),
	path("brands/", machines_brands, name = "machines_brands"),
	path("new-machiness-supply/", machines_new_machines_supply,
		name = "machines_new_machines_supply"),
	path("update-machines-supply-stock/<str:slug>", 
		update_machines_supply_stock, name = "update_machines_supply_stock"),
	path("machines-supply-detail/<str:slug>", machines_machines_supply_detail,
		name = "machines_machines_supply_detail"),
	path("delete-machines-supply/<str:slug>", machines_delete_machines_supply,
		name = "machines_delete_machines_supply"),
]