from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from utils import utils
from .models import *
from home.forms import *


class BrandForm(forms.ModelForm):
	class Meta:
		model = Brand
		fields = ["name"]

	name = custom_name_field


class MachineForm(forms.ModelForm):
	class Meta:
		model = Machine
		fields = "__all__"

	location = location_field
	brand = forms.ModelChoiceField(
		required = False,
		queryset = Brand.objects.all(),
		widget = forms.Select(
			attrs = {"class": "form-select"}
			),
		label = "Marca"
		)
	model = forms.CharField(
		required = False,
		max_length = 30,
		widget = text_field_widget,
		label = "Modelo",
		)
	name = custom_name_field
	code = code_field
	stock = stock_field
	description = optional_description_field


	def clean_name(self):
		name = self.cleaned_data.get('name')
		if not self.instance.pk:
			if Machine.objects.filter(name = name).exists():
				raise ValidationError("Ya existe un registro con ese nombre.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(name = name).exists():
					raise ValidationError(f"ya existe un registro en {item_model._meta.verbose_name_plural} con ese nombre.")

		return name				


	def clean_code(self):
		code = self.cleaned_data.get('code')
		if not self.instance.pk:
			if Machine.objects.filter(code = code).exists():
				raise ValidationError("Ya existe un registro con ese código.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(code = code).exists():
					raise ValidationError(f"Ya existe un registro en {item_model._meta.verbose_name_plural} con ese código.")

		return code

# Formulario para el suministro para máquinas

class MachinesSupplyForm(forms.ModelForm):

	class Meta:
		model = MachinesSupply
		fields = "__all__"

	location = location_field
	brand = forms.ModelChoiceField(
		required = False,
		queryset = Brand.objects.all(),
		widget = forms.Select(
			attrs = {"class": "form-select"}
			),
		label = "Marca (opcional)"
		)
	machine = forms.ModelChoiceField(
		required = False,
		queryset = Machine.objects.all(),
		widget = forms.Select(
			attrs = {"class": "form-select"}
			),
		label = "Máquina (opcional)"
		)
	name = custom_name_field
	code = code_field
	stock = stock_field
	description = optional_description_field

	def clean_name(self):
		name = self.cleaned_data.get('name')
		if not self.instance.pk:
			if MachinesSupply.objects.filter(name = name).exists():
				raise ValidationError("Ya existe un registro con ese nombre.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(name = name).exists():
					raise ValidationError(f"ya existe un registro en {item_model._meta.verbose_name_plural} con ese nombre.")

		return name				


	def clean_code(self):
		code = self.cleaned_data.get('code')
		if not self.instance.pk:
			if MachinesSupply.objects.filter(code = code).exists():
				raise ValidationError("Ya existe un registro con ese código.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(code = code).exists():
					raise ValidationError(f"Ya existe un registro en {item_model._meta.verbose_name_plural} con ese código.")

		return code