from django.db import models
from home.models import *
from utils import utils


class Brand(models.Model):
    name = models.CharField(max_length = 50, verbose_name = "Nombre")
    slug = utils.autoslug_field

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Marca"
        verbose_name_plural = "Marcas"
        db_table = "brand"
        ordering = ("name", )


class Machine(Item):
    brand = models.ForeignKey(Brand, on_delete = models.SET_NULL,
        null = True, verbose_name = "Marca")
    model = models.CharField(max_length = 50, verbose_name = "Modelo")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Máquina"
        verbose_name_plural = "Máquinas"
        db_table = "machine"


class MachinesSupply(Item):
    brand = models.ForeignKey(Brand, on_delete = models.SET_NULL,
        null = True, verbose_name = "Marca")
    machine = models.ForeignKey(Machine, on_delete = models.SET_NULL,
        null = True, verbose_name = "Máquina (opcional)")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Suministros para Máquinas"
        verbose_name_plural = "Suministros para Máquinas"
        db_table = "machines_supply"
