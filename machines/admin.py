from django.contrib import admin
from .models import *
from home.admin import ItemAdmin

# Register your models here.

class MachineAdmin(ItemAdmin):
	list_display = ["name", "code", "stock", "brand", "model", 
	"location"]


class MachinesSupplyAdmin(ItemAdmin):
	list_display = ["name", "code", "machine", "stock", "location"]


admin.site.register(Brand)
admin.site.register(Machine, MachineAdmin)
admin.site.register(MachinesSupply, MachinesSupplyAdmin)
