from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from utils import utils
from .models import *
from home.forms import *


class LocationForm(forms.ModelForm):

	class Meta:
		model = Location
		fields = ["name"]

	name = custom_name_field