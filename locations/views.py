from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .models import *
from .forms import *


### Vista para listar las localizaciones

@login_required
def locations_home(request):
	locations = Location.objects.all()

	for location in locations:
		location.item_count = 0
		for item_model in Item.__subclasses__():
			location.item_count += item_model.objects.filter(location = location.pk).count()

	context = {
		"title": "Sección de Localizaciones",
		"locations": locations
	}

	return render(request, "locations/locations_home.html",
		context)


### Vista para registrar una localización

@login_required
def locations_new_location(request):
	
	if request.method == "POST":
		form = LocationForm(request.POST or None)

		if form.is_valid():
			data = form.cleaned_data

			new_location = Location(
				name = data["name"]
			)

			new_location.save()

			messages.add_message(request, messages.SUCCESS,
				"Localización registrada correctamente")

			return HttpResponseRedirect(reverse('locations_home'))

	else:
		form = LocationForm()

	context = {
		"title": "Registrar nueva Localización",
		"form": form
	}

	return render(request, 'locations/locations_new_location.html',
		context)


### Vista para editar una localización

@login_required
def locations_edit_location(request, slug):
	
	location = get_object_or_404(Location, slug = slug)
	item_count = 0

	item_models = Item.__subclasses__()

	for item_model in item_models:
		item_count += item_model.objects.filter(location = location.pk).count()

	initial = {
		"name": location.name
	}

	if request.method == "POST":

		form = LocationForm(request.POST or None,
			initial = initial, instance = location)

		if form.is_valid():
			location.name = form.cleaned_data["name"]

			location.save()

			messages.add_message(request, messages.SUCCESS,
				"Localización renombrada correctamente.")

			return HttpResponseRedirect(reverse('locations_home'))

	else:

		form = LocationForm(instance = location)

	context = {
		"title": "Modificar localización",
		"location": location,
		"item_count": item_count,
		"form": form,
	}

	return render(request, 'locations/locations_edit_location.html',
		context)


### Vista para eliminar una localización

@login_required
def locations_delete_location(request, slug):
	
	location = get_object_or_404(Location, slug = slug)

	location.delete()

	messages.add_message(request, messages.INFO,
		"Se ha eliminado la localización")

	return HttpResponseRedirect(reverse('locations_home'))
