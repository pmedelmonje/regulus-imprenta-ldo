from django.db import models
from home.models import *
from utils import utils


class Location(models.Model):
    name = models.CharField(max_length=32, unique = True,
        verbose_name = "Name")
    slug = utils.autoslug_field

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = "Localización"
        verbose_name_plural = "Localizaciones"
        db_table = "location"
