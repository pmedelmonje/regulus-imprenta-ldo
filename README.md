# Proyecto Régulus 

Sistema de gestión de imprenta

## Índice

- [Configuración inicial](#configuración-inicialconfiguración-inicial)
- [Permisos y autenticación de usuarios](#permisos-y-autenticación-de-usuarios)

---

### Configuración inicial

#### `Requerimientos`

**Versión de Python:** 3.7 o posterior.

**Versión de Django:** 4.0 o posterior.

**Dependencias:**

~~~
Django
django-autoslug
django-environ
psycopg2-binary
pyTelegramBotAPI
~~~

Igualmente se puede acceder al directorio raíz en una terminal y ejecutar `pip install -r requirements.txt` (o `pip3` según el sistema operativo).

En caso de utilizar un gestor de Base de Datos distinto a PostgreSQL, `psycopg2-binary` se reemplaza por la librería correspondiente.

#### `Rutas permitidas en settings.py`

El archivo `settings.py` contiene una lista de nombre `ALLOWED_HOSTS`. Aquí se debe incluir las rutas configuradas para acceder a la web de Régulus cuando se despliegue en Producción. Mientras se ejecute en modo de depuración, no es necesario, pero en Producción, debe especificarse las URL permitidas para cargar la web (incluso si es `localhost`).

En un entorno de producción, debería lucir así por ejemplo:

~~~
ALLOWED_HOSTS = ['localhost', '127.0.0.1:8000']
~~~

#### `Variables de entorno`

El proyecto usa la librería `django-environ` para configurar las variables de entorno. Para el correcto funcionamiento de éstas en Régulus, se debe crear un archivo `.env` en el mismo directorio donde se encuentra el archivo `settings.py`. El archivo debe contener las siguientes variables y sus valores correspondientes:

~~~
DEBUG
SECRET_KEY
DB_NAME
DB_USER
DB_PASSWORD
DB_SERVER
DB_PORT
~~~

**DEBUG** debería tener el valor `False` cuando Régulus se despliegue en Producción. 

**SECRET_KEY** es requerida por Django para funcionar y se aconseja usar una distinta a la que viene por defecto. Se puede utilizar [este generador](https://djecrety.ir/) para obtener una clave secreta.

Las demás variables guardan los parámetros de la conexión a la Base de Datos y dependen del conector.

#### `Archivos estáticos`

Las plantillas HTML contienen tanto estilos propios de CSS como estilos de Bootstrap 5. Inicialmente, Bootstrap se importa por medio de su CDN, pero si Régulus se utilizará en un servidor local, se recomienda descargar sus archivos, de modo de no depender de una buena conexión a internet para que los estilos se carguen correctamente.

Para que las plantillas HTML se rendericen correctamente cuando se cambia a modo de producción, se debe ejecutar, al menos una vez, el siguiente comando en la terminal:

`python3 manage.py collectstatic` (o `python manage.py collectstatic`, dependiendo del sistema operativo)

#### `Migraciones`

En la constante `DATABASES` del archivo `settings.py` se especifica el gestor de Base de Datos por medio del llamado de una función. Justo arriba de esa constante, está comentada la forma de utilizar cada gestor, para una mayor comodidad. 

Se recuerda tener instalada la dependencia correspondiente para el gestor de Base de Datos que se vaya a utilizar. Por ejemplo, `psycopg2` o `psycopg2-binary` para PostgreSQL; y `mysqlclient` para MySQL (hay más conectores para trabajar con MySQL y Python, pero en la [documentación de Django](https://docs.djangoproject.com/en/5.0/ref/databases/#mysql-db-api-drivers) se recomienda `mysqlclient`).

---

### Permisos y autenticación de usuarios

#### `Autenticación`

El proyecto utiliza el sistema de autenticación de Django para proteger la mayoría de las rutas; principalmente aquellas donde se crean o actualizan registros, aunque es posible acceder a algunas rutas sin la necesidad de autenticarse. 

#### `Permisos de usuarios`

Los **usuarios regulares** autenticados tendrán acceso a la mayoría de las funcionalidades en Régulus, como crear registros y actualizarlos, pero no tendrán acceso al **Panel de Administración de Django**. Solo los **superusuarios** tendrán acceso al Panel de Administración, y podrán realizar tareas como registrar nuevos usuarios, actualizar sus datos o desactivarlos. Según los requerimientos de la organización, se determinará cuántos usuarios tendrán atribuciones de "superusuario".

De igual modo, habrá una **cuenta de usuario maestro**, que tendrá todas las atribuciones, incluida el convertir a usuarios en superusuarios. Por motivos de seguridad, habrá solo una cuenta de usuario maestro en el entorno del proyecto.