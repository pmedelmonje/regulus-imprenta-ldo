from django.conf import settings
from telebot import TeleBot # pip install pytelegrambotapi
from notifications.models import TelegramSetting


def send_bot_message(message):

    try:
        settings = TelegramSetting.objects.get(name = "main")
        if settings.is_active:   
            # None para texto plano. "Markdown" texto formateado
            bot = TeleBot(settings.bot_token, parse_mode="Markdown")   
            bot.send_message(settings.chat_id, message)
    
    except Exception as e:
        print(str(e))


# def send_bot_message(message):
#     BOT_TOKEN = settings.TELEGRAM_API_KEY
#     CHAT_ID = settings.TELEGRAM_USER_ID

#     if BOT_TOKEN is not None and CHAT_ID is not None:
#         bot = TeleBot(token = BOT_TOKEN)
#         bot.send_message(CHAT_ID, message, parse_mode= "Markdown")