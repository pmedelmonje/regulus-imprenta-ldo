from django.core.exceptions import ValidationError
from django.db import models
import re
from autoslug import AutoSlugField


### Utilidades y variables para los modelos

def name_validator(value):
	if len(value) < 3:
		raise ValidationError("El nombre debe tener al menos 3 caracteres")
	if re.match(r"^\s", value):
		raise ValidationError("El nombre no debe tener espacios iniciales")


def unique_name_validator(value, model):
	if model.objects.filter(name = value).exists():
		raise ValidationError('Ese nombre ya está siendo usado.')


def phone_number_validator(value):
	if not re.match(r"^\+?569\d{8}$", value):
		raise ValidationError("Formato de número telefónico inválido.")


name_field = models.CharField(
        max_length =128,
        unique = True,
        validators = [name_validator],
        verbose_name = "Nombre"
        )

autoslug_field = AutoSlugField(populate_from = "name", unique= True,
                        always_update = True)

phone_number_field = models.CharField(max_length = 12, unique = True,
						validators = [phone_number_validator], 
						verbose_name = "Teléfono")

optional_description_field = models.TextField(null = True, blank = True,
                        verbose_name = "Descripción")


### Utilidades para el Panel de Administración

def show_description(obj):
	if len(obj.description) > 30:
		return f"{obj.description[:31]}..."
	return obj.description

show_description.short_description = "Description"


def addition_or_substraction(previous_stock, new_stock):
	if previous_stock < new_stock:
		return "suma"
	elif previous_stock > new_stock:
		return "resta"