from django.contrib import messages
from django.shortcuts import redirect


def superuser_required(view):
	
	def wrapped_view(request, *args, **kwargs):
		if request.user.is_authenticated:

			if request.user.is_superuser:
				return view(request, *args, **kwargs)
			else:
				messages.add_message(request, messages.WARNING,
					"Solo un superusuario está autorizado a realizar esta acción")

		else:
			messages.add_message(request, messages.WARNING,
				"Se requiere inicio de sesión")

	return wrapped_view


