from django.contrib import admin
from .models import Record, RecordType
from utils import utils

# Register your models here.

@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
	list_display = ["record_type", "item", "created_on", utils.show_description]


@admin.register(RecordType)
class RecordTypeAdmin(admin.ModelAdmin):
	list_display = ["name", "slug"]