import locale
from rest_framework import serializers
from records.models import Record, RecordType
from items.models import Item
from items.api.serializers import ItemSerializer
from users.models import User
from users.api.serializers import UserSerializer

locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')


class RecordTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = RecordType
		fields = ['id','name', 'slug']


class CreateUpdateRecordTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = RecordType
		fields = ["name"]


class CreateUpdateRecordSerializer(serializers.ModelSerializer):
	user = serializers.ReadOnlyField()
	class Meta:
		model = Record
		fields = ['user', 'record_type', 'item', 'description']


class RecordSerializer(serializers.ModelSerializer):
	item = ItemSerializer()
	record_type = RecordTypeSerializer()
	user = UserSerializer()

	class Meta:
		model = Record
		fields = ['id', 'user', 'item', 'slug', 'record_type','created_on',
		'last_update', 'description']


class SearchRecordSerializer(serializers.Serializer):
	user = serializers.IntegerField(required = False)
	item = serializers.IntegerField(required = False)
	record_type = serializers.IntegerField(required = False)
	creation_year = serializers.CharField(max_length = 4,
				       required = False)
	creation_month = serializers.CharField(max_length = 2,
				       required = False)
	creation_day = serializers.CharField(max_length = 2,
				       required = False)