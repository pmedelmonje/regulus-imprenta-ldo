from django.urls import path
from .views import (RecordsAPIView, RecordTypesAPIView,
	RecordTypeAPIView, SearchRecordsAPIView, RecordAPIView)

urlpatterns = [
	path('record-types/', RecordTypesAPIView.as_view()),
	path('record-types/<str:slug>', RecordTypeAPIView.as_view()),
	path('records/', RecordsAPIView.as_view()),
    path('records/search/', SearchRecordsAPIView.as_view()),
    path('records/<str:slug>', RecordAPIView.as_view()),
]