from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema
from records.models import Record, RecordType
from .serializers import (RecordSerializer, RecordTypeSerializer,
	CreateUpdateRecordSerializer, CreateUpdateRecordTypeSerializer,
	SearchRecordSerializer)
from utils import utils
from utils.permissions import *
from utils.pagination import CustomPagination
from utils.telegram_bot import send_bot_message


class RecordTypesAPIView(APIView):

	permission_classes = [IsAuthenticated]

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: RecordTypeSerializer()}
	)
	def get(self, request, format = None):
		record_types = RecordType.objects.all()
		serializer = RecordTypeSerializer(record_types, many = True)
		return Response(serializer.data,
			status = status.HTTP_200_OK)

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		request_body = CreateUpdateRecordTypeSerializer(),
		responses = {201: RecordTypeSerializer()}
	)
	def post(self, request, format = None):
		serializer = CreateUpdateRecordTypeSerializer(data = request.data)
		if serializer.is_valid():
			record_type_instance = serializer.save()
			record_type_serializer = RecordTypeSerializer(
				instance = record_type_instance)
			return Response(
				data = record_type_serializer.data,
				status = status.HTTP_201_CREATED
			)
		return Response(serializer.errors,
			status = status.HTTP_400_BAD_REQUEST)


class RecordTypeAPIView(APIView):
	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: RecordTypeSerializer()}
	)
	def get(self, request, slug, format = None):
		try:
			record_type = RecordType.objects.get(slug = slug)
		except RecordType.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)

		serializer = RecordTypeSerializer(record_type)
		return Response(serializer.data,
			status = status.HTTP_200_OK)


	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		request_body = CreateUpdateRecordTypeSerializer(),
		responses = {200: RecordTypeSerializer()}
	)
	def patch(self, request, slug, format = None):
		try:
			record_type = RecordType.objects.get(slug = slug)
		except RecordType.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)

		serializer = CreateUpdateRecordTypeSerializer(record_type,
			data = request.data, partial = True)
		if serializer.is_valid():
			serializer.save()
			return Response({'response': 'Información actualizada'},
				status = status.HTTP_200_OK)
		return Response(serializer.errors,
			status = status.HTTP_400_BAD_REQUEST)


class RecordsAPIView(APIView):

	permission_classes = [IsAuthenticated]
	pagination_class = CustomPagination

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: RecordSerializer}
	)
	def get(self, request, format = None):
		records = Record.objects.all()
		serializer = RecordSerializer(records, many = True)
		return Response(serializer.data,
			status = status.HTTP_200_OK)


	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		request_body = CreateUpdateRecordSerializer(),
		responses = {201: RecordSerializer()}
	)
	def post(self, request, format = None):
		serializer = CreateUpdateRecordSerializer(data = request.data)
		if serializer.is_valid():
			record_instance = serializer.save(user = request.user)
			message = f"*{request.user.first_name} {request.user.last_name}* ha creado un registro en la bitácora de *{record_instance.item.name}*"
			send_bot_message(message)

			record_serializer = RecordSerializer(instance = record_instance)
			return Response(
				data = record_serializer.data,
				status = status.HTTP_201_CREATED
			)

		return Response(serializer.errors,
			status = status.HTTP_400_BAD_REQUEST
		)
	

class RecordAPIView(APIView):

	permission_classes = [IsAuthenticated, IsRecordOwnerOrReadOnly]

	@swagger_auto_schema(
		manual_parameters = [authorization_header],
		responses = {200: RecordSerializer()}
	)
	def get(self, request, slug, format = None):
		try:
			record = Record.objects.get(slug = slug)
		except Record.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)
		
		serializer = RecordSerializer(record)

		return Response(serializer.data,
		  status = status.HTTP_200_OK)
	
	@swagger_auto_schema(
		manual_parameters= [authorization_header],
		request_body = CreateUpdateRecordSerializer(),
		responses = {200: "Información actualizada correctamente"}
	)
	def patch(self, request, slug, format = None):
		try:
			record = Record.objects.get(slug = slug)
		except Record.DoesNotExist:
			return Response(status = status.HTTP_404_NOT_FOUND)
		
		serializer = CreateUpdateRecordSerializer(record, 
			data = request.data, partial = True)
		
		if serializer.is_valid(raise_exception = True):
			serializer.save()

			return Response({"response": "Información actualizada correctamente"},
		   status = status.HTTP_200_OK)
		
		return Response(serializer.errors,
		  status = status.HTTP_400_BAD_REQUEST)
	

class SearchRecordsAPIView(APIView):

	pagination_class = CustomPagination

	@swagger_auto_schema(
		request_body = SearchRecordSerializer(),
		responses = {200: RecordSerializer(many = True)}
	)
	def post(self, request, format = None):
		serializer = SearchRecordSerializer(data = request.data,
				      partial = True)
		if serializer.is_valid():
			user = serializer.validated_data.get('user')
			item = serializer.validated_data.get('item')
			record_type = serializer.validated_data.get('record_type')
			creation_year = serializer.validated_data.get('creation_year')
			creation_month = serializer.validated_data.get('creation_month')
			creation_day = serializer.validated_data.get('creation_day')

			records = Record.objects.all()

			if user is not None:
				records = records.filter(user = user)
			
			if item is not None:
				records = records.filter(item = item)

			if record_type is not None:
				records = records.filter(record_type = record_type)

			if creation_year is not None:
				records = records.filter(created_on__year = creation_year)

			if creation_month is not None:
				records = records.filter(created_on__month = creation_month)

			if creation_day is not None:
				records = records.filter(created_on__day = creation_day)

			if not records.exists():
				return Response({"response": "No se encontraron registros"},
		    		status = status.HTTP_200_OK)
			
			serialized_records = RecordSerializer(records, many = True)

			return Response(serialized_records.data,
		   			status = status.HTTP_200_OK)
		
		return Response(serializer.errors, 
		  status = status.HTTP_400_BAD_REQUEST)


