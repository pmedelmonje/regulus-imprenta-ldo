from django.db import models
from django.utils import timezone
from users.models import User
from items.models import Item
from autoslug import AutoSlugField


class RecordType(models.Model):
	name = models.CharField(max_length = 255,
						 verbose_name= 'Nombre')
	slug = AutoSlugField(populate_from = "name",
		always_update = True, unique = True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = "Tipo de Registro"
		verbose_name_plural = "Tipos de Registro"
		db_table = "record_type"


class Record(models.Model):

	user = models.ForeignKey(User, on_delete = models.PROTECT, 
						  verbose_name = "Usuario")
	record_type = models.ForeignKey(RecordType, on_delete = models.SET_NULL,
		null = True, verbose_name = "Tipo de registro")
	item = models.ForeignKey(Item, on_delete = models.PROTECT, 
						  verbose_name = "Item")
	created_on = models.DateTimeField(default = timezone.now,
								   verbose_name = "Creado en")
	last_update = models.DateTimeField(auto_now_add = True,
									verbose_name = "Últ. actualización")
	slug = AutoSlugField(populate_from = "created_on", unique = True)
	description = models.TextField(verbose_name = "Descripción")

	def __str__(self):
		return f"{self.created_on} - {self.item.name}"


	class Meta:
		verbose_name = "Registro"
		verbose_name_plural = "Registros"
		db_table = "record"