import uuid
import random
import string
from django.db import models
from django.utils import timezone
from autoslug import AutoSlugField
from users.models import User
from books.models import Book, BookMaterial
from machines.models import Machine, MachinesSupply
from unclassified_items.models import UnclassifiedItem
from utils import utils


def generate_code():
	while True:
		code = ''.join(random.choice(string.digits) for _ in range(12))
		if not CustomLog.objects.filter(code = code).exists():
			return code


PRIORITIES = (
	("baja", "Baja"),
	("normal", "Normal"),
	("alta", "Alta"),
)

ITEM_LOG_TYPES = (
	("creacion_item", "Creación de ítem"),
	("suma", "Stock añadido"),
	("resta", "Stock restado"),
	("actualizacion_datos", "Actualización de datos"),
)

CUSTOM_LOG_TYPES = (
	("ingreso_persona", "Ingreso de persona"),
	("salida_persona", "Salida de persona"),
	("inicio_jornada", "Inicio de jornada"),
	("termino_jornada", "Término de jornada"),
	("otros", "Otros / Sin clasificar")
)


class ItemLog(models.Model):

	user = models.ForeignKey(User, on_delete = models.SET_NULL,
		null = True, verbose_name = "Usuario")
	book = models.ForeignKey(Book, on_delete = models.SET_NULL,
		null = True, verbose_name = "Libro")
	book_material = models.ForeignKey(BookMaterial, 
		on_delete = models.SET_NULL, null = True,
		verbose_name = "Material para libros")
	machine = models.ForeignKey(Machine, on_delete = models.SET_NULL,
		null = True, verbose_name = "Máquina")
	machines_supply = models.ForeignKey(MachinesSupply, 
		on_delete = models.SET_NULL, null = True,
		verbose_name = "Suministro para Maquinaria")
	unclassified_item = models.ForeignKey(UnclassifiedItem,
		on_delete = models.SET_NULL, null = True,
		verbose_name = "Ítem sin clasificar")
	log_type = models.CharField(max_length = 128, choices = ITEM_LOG_TYPES,
		verbose_name = "Tipo de registro")
	previous_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock previo")
	updated_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock nuevo")
	created_on = models.DateTimeField(default = timezone.now,
		verbose_name = "Creación")


	def __str__(self):
		return f"{self.created_on}"


	class Meta:
		verbose_name = "Registro de ítems"
		verbose_name_plural = "Registros de ítems"
		db_table = "item_log"


class BookLog(models.Model):

	user = models.ForeignKey(User, on_delete = models.SET_NULL,
	null = True, verbose_name = "Usuario")
	book = models.ForeignKey(Book, on_delete = models.SET_NULL,
	null = True, verbose_name = "Libro")
	log_type = models.CharField(max_length = 128, choices = ITEM_LOG_TYPES,
		verbose_name = "Tipo de registro")
	previous_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock previo")
	updated_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock nuevo")
	created_on = models.DateTimeField(default = timezone.now,
		verbose_name = "Creación")


	def __str__(self):
		return f"{self.created_on}"


	class Meta:
		verbose_name = "Registro de Libro"
		verbose_name_plural = "Registros de Libro"
		db_table = "book_log"


class BookMaterialLog(models.Model):

	user = models.ForeignKey(User, on_delete = models.SET_NULL,
	null = True, verbose_name = "Usuario")
	book_material = models.ForeignKey(BookMaterial, on_delete = models.SET_NULL,
	null = True, verbose_name = "Material para Libros")
	log_type = models.CharField(max_length = 128, choices = ITEM_LOG_TYPES,
		verbose_name = "Tipo de registro")
	previous_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock previo")
	updated_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock nuevo")
	created_on = models.DateTimeField(default = timezone.now,
		verbose_name = "Creación")


	def __str__(self):
		return f"{self.created_on}"


	class Meta:
		verbose_name = "Registro de Material para Libros"
		verbose_name_plural = "Registros de Material para Libros"
		db_table = "book_material_log"


class MachineLog(models.Model):

	user = models.ForeignKey(User, on_delete = models.SET_NULL,
	null = True, verbose_name = "Usuario")
	machine = models.ForeignKey(Machine, on_delete = models.SET_NULL,
	null = True, verbose_name = "Máquina")
	log_type = models.CharField(max_length = 128, choices = ITEM_LOG_TYPES,
		verbose_name = "Tipo de registro")
	previous_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock previo")
	updated_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock nuevo")
	created_on = models.DateTimeField(default = timezone.now,
		verbose_name = "Creación")


	def __str__(self):
		return f"{self.created_on}"


	class Meta:
		verbose_name = "Registro de Máquinas"
		verbose_name_plural = "Registros de Máquinas"
		db_table = "machine_log"


class MachinesSupplyLog(models.Model):

	user = models.ForeignKey(User, on_delete = models.SET_NULL,
	null = True, verbose_name = "Usuario")
	machines_supply = models.ForeignKey(MachinesSupply, on_delete = models.SET_NULL,
	null = True, verbose_name = "Suministro para máquinas")
	log_type = models.CharField(max_length = 128, choices = ITEM_LOG_TYPES,
		verbose_name = "Tipo de registro")
	previous_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock previo")
	updated_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock nuevo")
	created_on = models.DateTimeField(default = timezone.now,
		verbose_name = "Creación")


	def __str__(self):
		return f"{self.created_on}"


	class Meta:
		verbose_name = "Registro de Suministros de Máquinas"
		verbose_name_plural = "Registros de Suministros de Máquinas"
		db_table = "machines_supply_log"


class UnclassifiedItemLog(models.Model):

	user = models.ForeignKey(User, on_delete = models.SET_NULL,
	null = True, verbose_name = "Usuario")
	unclassified_item = models.ForeignKey(UnclassifiedItem, on_delete = models.SET_NULL,
	null = True, verbose_name = "Ítem")
	log_type = models.CharField(max_length = 128, choices = ITEM_LOG_TYPES,
		verbose_name = "Tipo de registro")
	previous_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock previo")
	updated_stock = models.PositiveSmallIntegerField(
		default = 0, verbose_name = "Stock nuevo")
	created_on = models.DateTimeField(default = timezone.now,
		verbose_name = "Creación")


	def __str__(self):
		return f"{self.created_on}"


	class Meta:
		verbose_name = "Registro de Ítems sin clasificar"
		verbose_name_plural = "Registros de Ítems sin clasificar"
		db_table = "unclassified_item_log"


class CustomLogType(models.Model):

	name = models.CharField(max_length = 255, unique = True, 
						 verbose_name = "Nombre")
	index = models.PositiveSmallIntegerField(
		default = 1,
		help_text = "El valor de índice permite ordenar los tipos de registro según prioridad.", 
		verbose_name = "Índice"
		)
	in_use = models.BooleanField(
		default = True,
		help_text = "Se puede desactivar esta casilla si se quiere dejar de usar este tipo de registro.", 
		verbose_name = "En uso"
		)


	def __str__(self):
		return self.name
	

	class Meta:
		verbose_name = "Tipo de Registros personalizados"
		verbose_name_plural = "Tipos de Registros personalizados"
		db_table = "custom_log_type"
		ordering = ("index", "name")


class CustomLog(models.Model):

	user = models.ForeignKey(User, on_delete = models.SET_NULL,
		null = True, verbose_name = "Usuario")
	log_type = models.ForeignKey(CustomLogType, on_delete = models.DO_NOTHING,
							  verbose_name = "Tipo de registro")
	priority = models.CharField(max_length = 30, choices = PRIORITIES,
		default = "", verbose_name = "Prioridad")
	created_on = models.DateTimeField(default = timezone.now,
		verbose_name = "Creación")
	updated_on = models.DateTimeField(auto_now_add = True,
		verbose_name = "Última actualización")
	is_archived = models.BooleanField(default = False,
		verbose_name = "Archivado")
	description = models.TextField(verbose_name = "Descripción")
	

	def __str__(self):
		return f"Registro con ID {self.id}"


	class Meta:
		verbose_name = "Registro personalizado"
		verbose_name_plural = "Registros personalizados"
		db_table = "custom_log"
