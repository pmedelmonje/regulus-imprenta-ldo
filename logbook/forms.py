from django import forms
from .models import *


class CustomLogForm(forms.ModelForm):

	class Meta:
		model = CustomLog
		fields = ["log_type", "priority", "description"]

	log_type = forms.ModelChoiceField(
		queryset = CustomLogType.objects.filter(in_use = True).all(),
		widget = forms.Select(
			attrs = {"class": "form-select"}
			),
		label = "Tipo de registro"
		)
	priority = forms.ChoiceField(
		choices = PRIORITIES,
		widget = forms.Select(
			attrs = {"class": "form-select"}
			),
		label = "Prioridad"
		)
	description = forms.CharField(
		widget = forms.Textarea(
			attrs = {
				"class": "form-control",
				"rows": 4,
				}
			),
		label = "Descripción"
		)