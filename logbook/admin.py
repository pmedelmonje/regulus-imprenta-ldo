from django.contrib import admin
from .models import *

# Register your models here.

class ItemLogAdmin(admin.ModelAdmin):
	list_display = ["created_on", "log_type", "book", "book_material", "machine",
	"machines_supply", "unclassified_item", "user"]


class BookLogAdmin(admin.ModelAdmin):
	list_display = ["created_on", "log_type", "book", "user"]


class BookMaterialLogAdmin(admin.ModelAdmin):
	list_display = ["created_on", "log_type", "book_material", "user"]


class MachineLogAdmin(admin.ModelAdmin):
	list_display = ["created_on", "log_type", "machine", "user"]


class MachinesSupplyLogAdmin(admin.ModelAdmin):
	list_display = ["created_on", "log_type", "machines_supply", "user"]


class UnclassifiedItemLogAdmin(admin.ModelAdmin):
	list_display = ["created_on", "log_type", "unclassified_item", "user"]


class CustomLogTypeAdmin(admin.ModelAdmin):
	list_display = ["name", "index", "in_use"]


class CustomLogAdmin(admin.ModelAdmin):
	list_display = ["created_on", "id", "log_type", "priority", "user"]


#admin.site.register(ItemLog, ItemLogAdmin)
admin.site.register(CustomLogType, CustomLogTypeAdmin)
admin.site.register(CustomLog, CustomLogAdmin)
admin.site.register(BookLog, BookLogAdmin)
admin.site.register(BookMaterialLog, BookMaterialLogAdmin)
admin.site.register(MachineLog, MachineLogAdmin)
admin.site.register(MachinesSupplyLog, MachinesSupplyLogAdmin)
admin.site.register(UnclassifiedItemLog, UnclassifiedItemLogAdmin)