from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .models import CustomLog
from .forms import CustomLogForm
from utils.telegram_bot import send_bot_message
from utils.decorators import superuser_required

# Create your views here.

@login_required
def logbook_home(request):

	custom_logs = CustomLog.objects.filter(is_archived = False).all()
	archived_custom_logs = CustomLog.objects.filter(is_archived = True).all()

	context = {
		"title": "Sección de Bitácora",
		"custom_logs": custom_logs,
		"archived_custom_logs": archived_custom_logs,
	}

	return render(request, 'logbook/logbook_home.html',
		context)


# Vista para crear un registro personalizado
@login_required
def logbook_new_custom_log(request):

	if request.method == "POST":
		form = CustomLogForm(request.POST or None)

		if form.is_valid():
			data = form.cleaned_data

			new_custom_log = CustomLog(
				user = request.user,
				log_type = data["log_type"],
				priority = data["priority"],
				description = data["description"]
			)

			new_custom_log.save()

			messages.add_message(request, messages.SUCCESS,
			"Registro creado exitosamente")

			# enviar mensaje con el bot
			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha creado un registro personalizado de tipo *{new_custom_log.log_type.name}* en la *Bitácora*."
			send_bot_message(message_text)

			# redirigir a la página de inicio
			return HttpResponseRedirect(reverse('logbook_home'))

	else:
		form = CustomLogForm()

	context = {
		"title": "Crear nuevo registro",
		"form": form
	}

	return render(request, 'logbook/logbook_new_custom_log.html',
		context)


# Vista para ver/editar un registro personalizado
@login_required
def logbook_custom_log_detail(request, id):
	custom_log = get_object_or_404(CustomLog, id = id)

	context = {
		"title": "Información de Registro",
		"custom_log": custom_log
	}

	return render(request, 'logbook/logbook_custom_log_detail.html',
		context)


# Vista para archivar un registro personalizado
@superuser_required
def logbook_switch_custom_log_archived_status(request, id):
	custom_log = get_object_or_404(CustomLog, id = id)

	custom_log.is_archived = not custom_log.is_archived
	custom_log.save()
	print(custom_log.is_archived)

	if custom_log.is_archived:

		alert_message = "El registro se ha archivado correctamente."
		message_text = f"El Registro Personalizado con *ID {custom_log.id}* ha sido archivado, y ya no estará visible en la Bitácora."

	else:

		alert_message = "El registro se ha desarchivado correctamente."
		message_text = f"El Registro Personalizado con *ID {custom_log.id}* ha sido desarchivado, y vuelve a estar visible en la Bitácora."

	send_bot_message(message_text)

	messages.add_message(request, messages.INFO,
		alert_message)

	return HttpResponseRedirect(reverse('logbook_home'))

