from django.urls import path
from .views import *


urlpatterns = [
	path('', logbook_home, name = "logbook_home"),
	path('new-custom-log/', logbook_new_custom_log,
		name = "logbook_new_custom_log"),
	path('custom-log-detail/<int:id>', logbook_custom_log_detail,
		name = "logbook_custom_log_detail"),
	path("switch-custom-log-archived-status/<int:id>", 
		logbook_switch_custom_log_archived_status,
		name = "logbook_switch_custom_log_archived_status"),
]