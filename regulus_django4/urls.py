from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('accounts/', include('authentication.urls')),
    path('locations/', include('locations.urls')),
    path('books/', include('books.urls')),
    path('machines/', include('machines.urls')),
    path('unclassified-items/', include('unclassified_items.urls')),
    path('logbook/', include('logbook.urls')),
]
