import environ
from django.conf import settings

env = environ.Env()
environ.Env.read_env()

def set_sqlite_db():
	return {
	'ENGINE': 'django.db.backends.sqlite3',
    'NAME': settings.BASE_DIR / 'db.sqlite3',
}

def set_mysql_db():
	"""pip install mysqlclient"""
	return {
	'ENGINE': 'django.db.backends.mysql',
	'NAME': env('DB_NAME'),
	'USER': env('DB_USER'),
	'PASSWORD': env('DB_PASSWORD'),
	'HOST': env('DB_SERVER'),
	'PORT': env('DB_PORT'),
	'OPTIONS': {
		'autocommit': True,
	}
}

def set_postgresql_db():
	"""
	pip install psycopg2
	or
	pip install psycopg2-binary
	"""
	return {
	'ENGINE': 'django.db.backends.postgresql',
	'NAME': env('DB_NAME'),
	'USER': env('DB_USER'),
	'PASSWORD': env('DB_PASSWORD'),
	'HOST': env('DB_SERVER'),
	'PORT': env('DB_PORT'),
}