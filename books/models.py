from django.db import models
from home.models import *
from utils import utils

# Create your models here.

MONTH_CHOICES = (
        ('', '---'),
        ('enero', 'Enero'), ('febrero','Febrero'),
        ('marzo','Marzo'), ('abril','Abril'),
        ('mayo','Mayo'), ('junio','Junio'),
        ('julio','Julio'), ('agosto','Agosto'),
        ('septiembre','Septiembre'), ('octubre','Octubre'),
        ('noviembre','Noviembre'), ('diciembre','Diciembre'),
    )

class BookCategory(models.Model):
    name = utils.name_field
    slug = utils.autoslug_field
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = "Categoría de Libros"
        verbose_name_plural = "Categorías de Libros"
        db_table = "book_category"


class Book(Item):

    book_category = models.ForeignKey(BookCategory, 
                    on_delete = models.PROTECT, verbose_name = "Categoría")
    part = models.CharField(max_length = 30, null = True, blank = True,
                                default = "1", verbose_name = "Parte o Tomo")
    year = models.PositiveSmallIntegerField(null = True, blank = True,
                                verbose_name = "Año")
    month = models.CharField(max_length = 10, null = True, blank = True,
                             choices = MONTH_CHOICES, default = None,
                             verbose_name = "Mes")
    
    def __str__(self):
        return self.name
    
    
    def clean_stock(self):
        if self.stock < 0:
            raise ValidationError("El stock debe ser igual o mayor a cero.")
        
    
    def clean_month(self):
        if not self.year and self.month is not None:
            raise ValidationError("Debe ingresar un año para poder ingresar un mes")
        
    
    def clean(self):
        super().clean()
        self.clean_stock()
        self.clean_month()

    
    class Meta:
        verbose_name = "Libro"      
        verbose_name_plural = "Libros"
        db_table = "book"


class BookMaterial(Item):
    slug = utils.autoslug_field

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Material para libros"
        verbose_name_plural = "Materiales para libros"
        db_table = "book_material"
