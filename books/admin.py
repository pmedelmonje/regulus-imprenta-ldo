from django.contrib import admin
from .models import *
from home.admin import ItemAdmin

# Register your models here.

class BookAdmin(ItemAdmin):
	list_display = ["name", "code", "stock", "location", "part", 
	"year", "month"]


class BookMaterialAdmin(ItemAdmin):
	list_display = ["name", "code", "stock", "location"]
	

admin.site.register(BookCategory)
admin.site.register(Book, BookAdmin)
admin.site.register(BookMaterial, BookMaterialAdmin)
