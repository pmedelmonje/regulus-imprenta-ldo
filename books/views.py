from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .models import *
from logbook.models import BookLog, BookMaterialLog
from .forms import *
from utils.utils import addition_or_substraction
from utils.telegram_bot import send_bot_message


### Vista para listar libros y materiales

def books_home(request):

	books = Book.objects.all()
	book_materials = BookMaterial.objects.all()

	context = {
		"title": "Sección Literatura",
		"books": books,
		"book_materials": book_materials,
	}

	return render(request, 'books/books_home.html',
		context)


### Vista para listar las categorías de libros

@login_required
def books_book_categories(request):

	book_categories = BookCategory.objects.all()

	for book_category in book_categories:
		book_category.book_count = Book.objects.filter(book_category = book_category).count()

	context = {
		"title": "Sección de Categorías",
		"book_categories": book_categories
	}

	return render(request, "books/books_book_categories.html",
		context)


### Vista para registrar una categoría de libros

@login_required
def books_new_book_category(request):
	
	if request.method == "POST":

		form = BookCategoryForm(request.POST or None)

		if form.is_valid():

			data = form.cleaned_data

			new_book_category = BookCategory(
				name = data["name"]
			)

			new_book_category.save()

			messages.add_message(request, messages.SUCCESS,
				"Categoría creada exitosamente.")

			return HttpResponseRedirect(reverse('books_book_categories'))

	else:

		form = BookCategoryForm()

	context = {
		"title": "Crear nueva Categoría de Libros",
		"form": form
	}

	return render(request, 'books/books_new_book_category.html',
		context)


### Vista para editar una categoría de libros

@login_required
def books_edit_book_category(request, slug):

	book_category = get_object_or_404(BookCategory, slug = slug)
	books = Book.objects.filter(book_category = book_category).count()


	if request.method == "POST":

		form = BookCategoryForm(
		request.POST, instance = book_category,
		initial = {"name": book_category.name}
		)

		if form.is_valid():
			data = form.cleaned_data
			book_category.name = data["name"]
			book_category.save()

			messages.add_message(request, messages.SUCCESS,
				"Registro modificado correctamente.")

			return HttpResponseRedirect(reverse('books_book_categories'))

	else:

		form = BookCategoryForm(instance = book_category)

	context = {
		"title": "Edición de Categoría",
		"form": form,
		"book_category": book_category,
		"books": books,
	}

	return render(request, 'books/books_edit_book_category.html',
		context)


### Vista para eliminar una categoría de libros

@login_required
def books_delete_book_category(request, slug):
	
	try:
		book_category = BookCategory.objects.get(slug = slug)
		book_category_name = book_category.name
	except BookCategory.DoesNotExist:
		raise Http404

	book_category.delete()

	messages.add_message(request, messages.INFO,
		f"Categoría {book_category_name} eliminada correctamente")

	return HttpResponseRedirect(reverse('books_book_categories'))


@login_required
def books_new_book(request):

	if request.method == "POST":

		form = BookForm(request.POST or None)

		if form.is_valid():
			data = form.cleaned_data

			new_book = Book(
				book_category = data["book_category"],
				location = data["location"],
				name = data["name"],
				code = data["code"],
				stock = data["stock"],
				part = data["part"] if data["part"] else "1",
				year = data["year"],
				month = data["month"],
				description = data["description"]
			)

			new_book.save()

			new_book_log = BookLog(
				user = request.user,
				book = new_book,
				log_type = "creacion_item",
				previous_stock = 0,
				updated_stock = new_book.stock
			)

			new_book_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha registrado en el inventario el ítem *{new_book.name}* en la sección de *Literatura*, con un stock inicial de *{new_book.stock}*."

			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
				f"Libro {new_book.name} registrado correctamente")

			return HttpResponseRedirect(reverse('books_home'))

	else:

		form = BookForm()

	context = {
		"title": "Registrar nuevo Libro",
		"form": form
	}

	return render(request, 'books/books_new_book.html',
		context)


@login_required
def update_book_stock(request, slug):
	book = get_object_or_404(Book, slug = slug)
	previous_stock = book.stock

	if request.method == "POST":
		stock_update = int(request.POST.get("stock_update"))
		print(stock_update)
		
		if stock_update == 0:
			messages.add_message(request, messages.WARNING,
				"Por favor, ingresar un valor distinto a cero.")


		elif book.stock + stock_update < 0:
			messages.add_message(request, messages.WARNING,
				"El stock final debe ser igual o mayor a cero.")

		else:

			book.stock = book.stock + stock_update

			book.save()

			new_book_log = BookLog(
				user = request.user,
				book = book,
				log_type = addition_or_substraction(previous_stock, book.stock),
				previous_stock = previous_stock,
				updated_stock = book.stock
			)

			new_book_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha actualizado el stock de *{book.name}*.\nStock previo: *{previous_stock}*.\nNuevo stock: *{book.stock}*."

			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
					f"Stock de {book.name} actualizado correctamente.")

	return HttpResponseRedirect(reverse('books_home'))


@login_required
def books_book_detail(request, slug):

	book = get_object_or_404(Book, slug = slug)
	logs = BookLog.objects.filter(book = book)

	context = {
		"title": "Información de",
		"book": book,
		"logs": logs,
	}

	return render(request, "books/books_book_detail.html",
		context)


@login_required
def books_delete_book(request, slug):
	book = get_object_or_404(Book, slug = slug)
	book_name = book.name

	book.delete()

	message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha eliminado el registro de *{book.name}*."
	
	send_bot_message(message_text)

	messages.add_message(request, messages.INFO,
		f"Se ha eliminado el registro {book_name} correctamente.")

	return HttpResponseRedirect(reverse('books_home'))


@login_required
def books_new_book_material(request):

	if request.method == "POST":

		form = BookMaterialForm(request.POST or None)

		if form.is_valid():
			data = form.cleaned_data

			new_book_material = BookMaterial(
				location = data["location"],
				name = data["name"],
				code = data["code"],
				stock = data["stock"],
				description = data["description"]
			)

			new_book_material.save()

			new_book_material_log = BookMaterialLog(
				user = request.user,
				book_material = new_book_material,
				log_type = "creacion_item",
				previous_stock = 0,
				updated_stock = new_book_material.stock
			)

			new_book_material_log.save()

			messages.add_message(request, messages.SUCCESS,
				"Material registrado correctamente")

			return HttpResponseRedirect(reverse('books_home'))

	else:

		form = BookMaterialForm()

	if form.errors.get('code'):
		print("Hay un error con el código")

	context = {
		"title": "Registrar nuevo Material",
		"form": form
	}

	return render(request, 'books/books_new_book_material.html',
		context)


@login_required
def update_book_material_stock(request, slug):
	book_material = get_object_or_404(BookMaterial, slug = slug)
	previous_stock = book_material.stock

	if request.method == "POST":
		stock_update = int(request.POST.get("stock_update"))
		
		if stock_update == 0:
			messages.add_message(request, messages.WARNING,
				"Por favor, ingresar un valor distinto a cero.")

		elif book_material.stock + stock_update < 0:
			messages.add_message(request, messages.WARNING,
				"El stock final debe ser igual o mayor a cero.")

		else:

			book_material.stock = book_material.stock + stock_update

			book_material.save()

			new_book_material_log = BookMaterialLog(
				user = request.user,
				book_material = book_material,
				log_type = addition_or_substraction(previous_stock,
					book_material.stock),
				previous_stock = previous_stock,
				updated_stock = book_material.stock
			)

			new_book_material_log.save()

			message_text = f"El usuario *{request.user.first_name} {request.user.last_name}* ha actualizado el stock del ítem *{book_material}*.\nStock anterior: *{previous_stock}*.\nStock nuevo: *{book_material.stock}*."
			send_bot_message(message_text)

			messages.add_message(request, messages.SUCCESS,
					f"Stock de {book_material.name} actualizado correctamente.")

	return HttpResponseRedirect(reverse('books_home'))


@login_required
def books_book_material_detail(request, slug):
	
	book_material = get_object_or_404(BookMaterial, slug = slug)
	logs = BookMaterialLog.objects.filter(book_material = book_material)

	context = {
		"title": "Información del Material",
		"book_material": book_material,
		"logs": logs,
	}

	return render(request, "books/books_book_material_detail.html",
		context)


@login_required
def books_delete_book_material(request, slug):

	book_material = get_object_or_404(BookMaterial, slug = slug)
	book_material_name = book_material.name

	book_material.delete()

	messages.add_message(request, messages.INFO,
		f'Material "{book_material_name}" eliminado correctamente.')

	return HttpResponseRedirect(reverse('books_home'))