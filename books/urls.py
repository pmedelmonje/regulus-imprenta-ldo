from django.urls import path
from .views import *

urlpatterns = [
	path("", books_home, name = "books_home"),
	path("book-categories/", books_book_categories,
		name = "books_book_categories"),
	path('new-book/', books_new_book, name = "books_new_book"),
	path('book-detail/<str:slug>', books_book_detail,
		name = "books_book_detail"),
	path('update-book-stock/<str:slug>', update_book_stock,
		name = "update_book_stock"),
	path('delete-book/<str:slug>', books_delete_book,
		name = "books_delete_book"),
	path('new-book-material', books_new_book_material,
		name = "books_new_book_material"),
	path('book-material-detail/<str:slug>', books_book_material_detail,
		name = "books_book_material_detail"),
	path('update-book-material-stock/<str:slug>', update_book_material_stock,
		name = "update_book_material_stock"),
	path('delete-book-material/<str:slug>', books_delete_book_material,
		name = "books_delete_book_material"),
]