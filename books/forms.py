from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from utils import utils
from .models import *
from home.forms import *


class BookCategoryForm(forms.ModelForm):

	class Meta:
		model = BookCategory
		fields = ["name"]

	name = custom_name_field

	def clean_name(self):
		name = self.cleaned_data.get('name')
		if not self.instance.pk:
			if BookCategory.objects.filter(name = name).exists():
				raise ValidationError("Ya existe un registro con ese nombre.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(name = name).exists():
					raise ValidationError(f"ya existe un registro en {item_model._meta.verbose_name_plural} con ese nombre.")

		return name


class BookForm(forms.ModelForm):

	class Meta:
		model = Book
		fields = '__all__'

	book_category = forms.ModelChoiceField(
		queryset = BookCategory.objects.all(),
		widget = forms.Select(
			attrs = {"class": "form-select"}),
		label = "Categoría"
		)
	location = location_field
	name = forms.CharField(
		max_length = 128, 
		widget = text_field_widget,
		validators = [
			utils.name_validator, 
			],
		label = "Nombre o Título"
		)
	code = code_field
	stock = stock_field
	part = forms.CharField(
		required = False,
		max_length = 30,
		widget = text_field_widget,
		label = "Parte o Tomo"
		)
	year = forms.IntegerField(
		required = False,
		widget = forms.NumberInput(
			attrs = {"class": "form-control"}),
		label = "Año"
		)
	month = forms.ChoiceField(
		required = False,
		choices = MONTH_CHOICES,
		widget = forms.Select(
			attrs = {"class": "form-select"}),
		label = "Mes"
		)
	description = optional_description_field


	def clean_name(self):
		name = self.cleaned_data.get('name')
		if not self.instance.pk:
			if Book.objects.filter(name = name).exists():
				raise ValidationError("Ya existe un registro con ese nombre.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(name = name).exists():
					raise ValidationError(f"ya existe un registro en {item_model._meta.verbose_name_plural} con ese nombre.")

		return name				


	def clean_code(self):
		code = self.cleaned_data.get('code')
		if not self.instance.pk:
			if Book.objects.filter(code = code).exists():
				raise ValidationError("Ya existe un registro con ese código.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(code = code).exists():
					raise ValidationError(f"Ya existe un registro en {item_model._meta.verbose_name_plural} con ese código.")

		return code


	def clean_month(self):
		year = self.cleaned_data.get('year')
		month = self.cleaned_data.get('month')

		if year is None and month != "":
			raise ValidationError("Se debe ingresar un año para poder ingresar un mes.")


class UpdateStockForm(forms.Form):

	stock_update = forms.IntegerField(
		required = False,
		widget = forms.NumberInput(
			attrs = {"class": "form-control w-80"}
			)
		)


class BookMaterialForm(forms.ModelForm):
	class Meta:
		model = BookMaterial
		fields = "__all__"

	location = location_field
	name = custom_name_field
	code = code_field
	stock = stock_field
	description = optional_description_field


	def clean_name(self):
		name = self.cleaned_data.get('name')
		if not self.instance.pk:
			if BookMaterial.objects.filter(name = name).exists():
				raise ValidationError("Ya existe un registro con ese nombre.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(name = name).exists():
					raise ValidationError(f"ya existe un registro en {item_model._meta.verbose_name_plural} con ese nombre.")

		return name				


	def clean_code(self):
		code = self.cleaned_data.get('code')
		if not self.instance.pk:
			if BookMaterial.objects.filter(code = code).exists():
				raise ValidationError("Ya existe un registro con ese código.")

			item_models = Item.__subclasses__()
			for item_model in item_models:
				if item_model.objects.filter(code = code).exists():
					raise ValidationError(f"Ya existe un registro en {item_model._meta.verbose_name_plural} con ese código.")

		return code
