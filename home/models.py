from django.db import models
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from autoslug import AutoSlugField
from users.models import User
from locations.models import Location
from utils import utils

# Create your models here.

class StockValidationMixin:
    def clean_stock(self):
        if self.stock < 0:
            raise ValidationError("El stock debe ser igual o mayor a cero.")


class Item(models.Model, StockValidationMixin):
    location = models.ForeignKey(Location, on_delete = models.SET_NULL,
        null = True, verbose_name = "Localización")
    code = models.CharField(max_length = 36, unique = True, 
        verbose_name = "Código")
    name = utils.name_field
    slug = utils.autoslug_field
    stock = models.PositiveSmallIntegerField(default = 0, 
                                verbose_name = "Stock")
    description = utils.optional_description_field
    
    class Meta:
        abstract = True


class ContactPerson(models.Model):

    ROLES = (
        ("trabajador", "Trabajador"),
        ("encargado", "Encargado(a)"),
        ("administrador", "Administrador"),
        ("programador", "Programador")
    )

    name = utils.name_field
    role = models.CharField(max_length = 32, choices = ROLES,
    verbose_name = "Rol")
    email = models.EmailField(verbose_name = "Email")
    phone_number = utils.phone_number_field
    phone_available = models.BooleanField(default = False,
                    verbose_name = "Puede recibir llamadas")
    whatsapp_available = models.BooleanField(default = False, 
                    verbose_name = "Tiene WhatsApp")
    telegram_available = models.BooleanField(default = False,
                    verbose_name = "Tiene Telegram")

    
    def __str__(self):
        return self.name


    class Meta:
        verbose_name = "Persona de contacto"
        verbose_name_plural = "Personas de contacto"
        db_table = "contact_person"
        ordering = ("role", )
    