from django.contrib import admin
from .models import Item, ContactPerson

# Register your models here.

class ItemAdmin(admin.ModelAdmin):
	list_display = ["code", "name", "stock", "location"]


class ContactPersonAdmin(admin.ModelAdmin):
	list_display = ["name", "role", "phone_number"]


admin.site.register(ContactPerson, ContactPersonAdmin)