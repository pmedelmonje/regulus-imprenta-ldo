from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.conf import settings
from .models import *
from books.models import *
from machines.models import *
from unclassified_items.models import *
from .forms import *


### Vista raíz del inventario

def home_index(request):

    literature_items_count = (Book.objects.all().count() + 
        BookMaterial.objects.all().count())

    machinery_items_count = (Machine.objects.all().count() +
        MachinesSupply.objects.all().count())

    unclassified_items_count = UnclassifiedItem.objects.all().count()
    
    context = {
        "title": "Inicio - Regulus Web",
        "literature_items_count": literature_items_count,
        "machinery_items_count": machinery_items_count,
        "unclassified_items_count": unclassified_items_count,
    }

    return render(request, 'home/home_index.html',
        context)


def home_telegram(request):

    context = {
        "title": "Notificaciones por Telegram"
    }

    return render(request, 'home/home_telegram.html', context)



def home_contact(request):

    contact_persons = ContactPerson.objects.all()

    context = {
        "title": "Contacto",
        "contact_persons": contact_persons
    }

    return render(request, 'home/home_contact.html', context)