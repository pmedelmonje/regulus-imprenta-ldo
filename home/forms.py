from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from utils import utils
from .models import *


def validate_stock(value):
	if value < 0:
		raise ValidationError('El stock no debe tener un valor negativo.')


text_field_widget = forms.TextInput(attrs = {'class': 'form-control'})

text_area_widget = forms.Textarea(attrs = 
	{'class': 'form-control', 'rows': 4}
	)

custom_name_field = forms.CharField(
	max_length = 128, 
	widget = text_field_widget,
	validators = [utils.name_validator],
	label = "Nombre")

code_field = forms.CharField(
	max_length = 30,
	widget = text_field_widget,
	validators = [MinLengthValidator(4)],
	label = "Código"
	)

stock_field = forms.IntegerField(
	validators = [validate_stock],
	widget = forms.NumberInput(
		attrs = {"class": "form-control"}),
	label = "Stock"
	)

optional_description_field = forms.CharField(required= False,
		widget= text_area_widget, label= 'Descripción')

location_field = forms.ModelChoiceField(
		queryset = Location.objects.all(),
		widget = forms.Select(
			attrs = {'class': 'form-select'}),
		label = "Localización"
		)
