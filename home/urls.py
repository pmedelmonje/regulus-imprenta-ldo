from django.urls import path
from .views import *

urlpatterns = [
    path('', home_index, name = 'home_index'),
    path('telegram', home_telegram, name = 'home_telegram'),
    path('contact', home_contact, name = 'home_contact')
]
