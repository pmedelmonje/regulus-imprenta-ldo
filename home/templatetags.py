from django import template
from datetime import datetime

register = template.Library()

@register.filter("formatted_datetime")
def formatted_datetime(datetime):
	return datetime.strftime('%Y/%b/%d - %H:%M:%S')